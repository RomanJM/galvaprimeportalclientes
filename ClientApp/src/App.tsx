import * as React from 'react';
import { Route, Switch } from 'react-router';
import LogIn from './components/common/LogIn';
import RecuperaPassword from './components/common/RecuperaPassword';
import NuevoPassword from './components/common/NuevoPassword';
import CambiarPassword from './components/common/CambiarPassword';
import Home from './components/Home';
import Inventarios from './components/pages/Inventarios';
import SeguimientoOC from './components/pages/SeguimientoOC';
import SeguimientoOCDetalle from './components/pages/SeguimientoOCDetalle';
import InventariosDetalle from './components/pages/InventariosDetalle';
import Embarques from './components/pages/Embarques';
import EmbarquesDetalle from './components/pages/EmbarquesDetalle';
import EstadoCuenta from './components/pages/EstadoCuenta';
import EstadoCuentaDetalle from './components/pages/EstadoCuentaDetalle';
import Facturas from './components/pages/Facturas';
import Remisiones from './components/pages/Remisiones';
import Dashboard from './components/pages/Dashboard';
import Certificados from './components/pages/Certificados';
import Contactos from './components/pages/Contactos';
import Conozcanos from './components/pages/Conozcanos';
import AvisoPrivacidad from './components/pages/AvisoPrivacidad';
import TerminosCondiciones from './components/pages/TerminosCondiciones';
import Layout from './components/Layout';
export default () => (
    
    <Switch>
        <Route exact path='/' component={LogIn} />
        <Route path='/recuperapassword' component={RecuperaPassword} />
        <Route path='/NuevoPassword' component={NuevoPassword} />
        <Layout>
            <Route path='/home' component={Home} />
            <Route path='/CambiarPassword' component={CambiarPassword} />
            <Route path='/SeguimientoOC' component={SeguimientoOC} />
            <Route path='/SeguimientoOCDetalle' component={SeguimientoOCDetalle} />
            <Route path='/Inventarios' component={Inventarios} />
            <Route path='/InventariosDetalle' component={InventariosDetalle} />
            <Route path='/Embarques' component={Embarques} />
            <Route path='/EmbarquesDetalle' component={EmbarquesDetalle} />
            <Route path='/EstadoCuenta' component={EstadoCuenta} />
            <Route path='/EstadoCuentaDetalle' component={EstadoCuentaDetalle} />
            <Route path='/Facturas' component={Facturas} />
            <Route path='/Remisiones' component={Remisiones} />
            <Route path='/Dashboard' component={Dashboard} />
            <Route path='/Certificados' component={Certificados} />
            <Route path='/Contactos' component={Contactos} />
            <Route path='/AvisoPrivacidad' component={AvisoPrivacidad} />
            <Route path='/Conozcanos' component={Conozcanos} />
            <Route path='/TerminosCondiciones' component={TerminosCondiciones} />
        </Layout>
    </Switch>
  
);
