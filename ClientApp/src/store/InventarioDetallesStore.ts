﻿import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';
import { fetch, addTask } from 'domain-task';
import axios from 'axios';
import * as _ from 'lodash';
import * as url from '../common';

export interface InventarioDetallesStoreState {
    isloading: boolean,
    registros: any[]
}

export interface RequestInformacionInicialInventarioDetalle {
    type: 'REQUEST_INFORMACION_INICIAL_INVENTARIO_DETALLES'
}

export interface ReceiveInformacionInicialInventarioDetalle {
    type: 'RECEIVE_INFORMACION_INICIAL_INVENTARIO_DETALLES',
    registros: any[]
}

type KnownAction = RequestInformacionInicialInventarioDetalle | ReceiveInformacionInicialInventarioDetalle;

export const actionCreators = {
    requestInformacionInicialInventarioDeta: (mercado: string, cliente: any, vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({ type: 'REQUEST_INFORMACION_INICIAL_INVENTARIO_DETALLES' });
        var data_str = {
            CardCode: cliente ? cliente : '',
            CodAlmacen: mercado,
            SlpCode: vendedor ? vendedor : 0
        };
        let fetchTask = axios.post(url.InventarioDetalle, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                console.log(data.OData);
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_INVENTARIO_DETALLES', registros: data.OData });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_INVENTARIO_DETALLES', registros: [] });
                console.log(error);
            })

        addTask(fetchTask);
    },
    requestDowloadExcel: (mercado: string, cliente: any, vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            CardCode: cliente ? cliente : '',
            CodAlmacen: mercado,
            SlpCode: vendedor ? vendedor : 0
        };
        var respnseA = axios.post(url.InventarioExcel, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            },
            responseType: 'blob'
        })
            .then(response => {
                //let archivo = response.headers["content-disposition"];
                //let tmp = archivo.split("=");
                //archivo = tmp[1];
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', "Inventarios.xls");
                document.body.appendChild(link);
                link.click();
            })
            .catch(error => {
                console.log(error);
            })
    }
};

const unloadedState: InventarioDetallesStoreState = {
    isloading: false,
    registros: []
};

export const reducer: Reducer<InventarioDetallesStoreState> = (state: InventarioDetallesStoreState | undefined, incomingAction: Action): InventarioDetallesStoreState => {
    if (state === undefined) {
        return unloadedState;
    }
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_INFORMACION_INICIAL_INVENTARIO_DETALLES':
            return {
                ...state,
                isloading: true,
                registros: []
            };
        case 'RECEIVE_INFORMACION_INICIAL_INVENTARIO_DETALLES':
            return {
                ...state,
                isloading: false,
                registros: action.registros
            };

        default:
            return state;
    }
};

