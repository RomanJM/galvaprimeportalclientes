
import * as SeguimientoOCStore from './SeguimientoOCStore';
import * as SeguimientoOCDetalleStore from './SeguimientoOCDetalleStore';
import * as InventariosStore from './InventariosStore';
import * as InventarioDetallesStore from './InventarioDetallesStore';
import * as EmbarquesStore from './EmbarquesStore';
import * as EmbarqueDetallesStore from './EmbarqueDetallesStore';
import * as EstadoCuentaStore from './EstadoCuentaStore';
import * as EstadoCuentaDetalleStore from './EstadoCuentaDetalleStore';
import * as FacturasStore from './FacturasStore';
import * as RemisionesStore from './RemisionesStore';
import * as DashboardStore from './DashboardStore';
import * as CertificadosStore from './CertificadosStore';
import * as ContactosStore from './ContactosStore';
// The top-level state object
export interface ApplicationState {
    SeguimientoOCStore: SeguimientoOCStore.SeguimientoOCStoreState,
    SeguimientoOCDetalleStore: SeguimientoOCDetalleStore.SeguimientoOCDetalleStoreState,
    InventariosStore: InventariosStore.InventariosStoreState,
    InventarioDetallesStore: InventarioDetallesStore.InventarioDetallesStoreState
    EmbarquesStore: EmbarquesStore.EmbarquesStoreState,
    EmbarqueDetallesStore: EmbarqueDetallesStore.EmbarqueDetallesStoreState
    EstadoCuentaStore: EstadoCuentaStore.EstadoCuentaStoreState,
    EstadoCuentaDetalleStore: EstadoCuentaDetalleStore.EstadoCuentaDetalleStoreState,
    FacturasStore: FacturasStore.FacturasStoreState,
    RemisionesStore: RemisionesStore.RemisionesStoreState,
    DashboardStore: DashboardStore.DashboardStoreState,
    CertificadosStore: CertificadosStore.CertificadosStoreState,
    ContactosStore: ContactosStore.ContactosStoreState,

}

// Whenever an action is dispatched, Redux will update each top-level application state property using
// the reducer with the matching name. It's important that the names match exactly, and that the reducer
// acts on the corresponding ApplicationState property type.
export const reducers = {
    SeguimientoOCStore: SeguimientoOCStore.reducer,
    SeguimientoOCDetalleStore: SeguimientoOCDetalleStore.reducer,
    InventariosStore: InventariosStore.reducer,
    InventarioDetallesStore: InventarioDetallesStore.reducer,
    EmbarquesStore: EmbarquesStore.reducer,
    EmbarqueDetallesStore: EmbarqueDetallesStore.reducer,
    EstadoCuentaStore: EstadoCuentaStore.reducer,
    EstadoCuentaDetalleStore: EstadoCuentaDetalleStore.reducer,
    FacturasStore: FacturasStore.reducer,
    RemisionesStore: RemisionesStore.reducer,
    DashboardStore: DashboardStore.reducer,
    CertificadosStore: CertificadosStore.reducer,
    ContactosStore: ContactosStore.reducer
};

// This type can be used as a hint on action creators so that its 'dispatch' and 'getState' params are
// correctly typed to match your store.
export interface AppThunkAction<TAction> {
    (dispatch: (action: TAction) => void, getState: () => ApplicationState): void;
}
