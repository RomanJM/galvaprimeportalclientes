﻿import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';
import axios from 'axios';
import * as _ from 'lodash';
import * as url from '../common';

export interface DashboardStoreState {
    isloading: boolean,
    registros: any,
    clientes: any[],
    vendedores: any[]
}

export interface RequestInformacionInicial {
    type: 'REQUEST_INFORMACION_INICIAL_DASHBOARD'
}

export interface ReceiveInformacionInicial {
    type: 'RECEIVE_INFORMACION_INICIAL_DASHBOARD',
    registros: any
}
export interface ReceiveInformacionClientes {
    type: 'RECEIVE_CLIENTES',
    clientes: any[]
}
export interface ReceiveInformacionVendedores {
    type: 'RECEIVE_VENDEDORES',
    vendedores: any[]
}


type KnownAction = RequestInformacionInicial | ReceiveInformacionInicial | ReceiveInformacionClientes | ReceiveInformacionVendedores;

export const actionCreators = {
    requestInformacionInicial: (cliente: string, vendedor: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({ type: 'REQUEST_INFORMACION_INICIAL_DASHBOARD' });
        var data_str = {
            CardCode: cliente && cliente != "0" ? cliente : '',
            SlpCode: vendedor ? vendedor : 0
        };
        axios.post(url.Dashboard, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                console.log(data);
                console.log(data.Message2);

                var localstorage = window.localStorage;
                localstorage.removeItem("FechaActualizacion");
                localstorage.setItem("FechaActualizacion", data.Message2);

                
                //var registros = _.filter(data.OData, { Vencimiento: vencimiento });        
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_DASHBOARD', registros: data.OData });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_DASHBOARD', registros: [] });
                console.log(error);
            })
    },
    requestClientes: (vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var clientes: any = [];
        axios.post(url.ClientesGet, { SlpCode: vendedor ? vendedor : 0 }, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    clientes = data.OData.map((x: any) => {
                        if (x.EsActivo)
                            return { "clave": x.CardCode, "descripcion": x.Name, "rfc": x.Rfc }
                    });
                    clientes = _.orderBy(clientes, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_CLIENTES', clientes: data.Type == "success" ? clientes : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_CLIENTES', clientes: [] });
                console.log(error);
            })

    },
    requestFiltraClientes: (vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            SlpCode: parseInt(vendedor)
        };
        var clientes: any = [];
        axios.post(url.ClientesGet, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    clientes = data.OData.map((x: any) => {
                        if (x.EsActivo)
                            return { "clave": x.CardCode, "descripcion": x.Name, "rfc": x.Rfc }
                    });
                    clientes = _.orderBy(clientes, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_CLIENTES', clientes: data.Type == "success" ? clientes : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_CLIENTES', clientes: [] });
                console.log(error);
            })

    },
    requestVendedores: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var vendedores: any = [];
        axios.post(url.VendedorGet, {}, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    vendedores = data.OData.map((x: any) => {
                        return { "clave": x.SlpCode, "descripcion": x.SlpName }
                    });
                    vendedores = _.orderBy(vendedores, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_VENDEDORES', vendedores: data.Type == "success" ? vendedores : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_VENDEDORES', vendedores: [] });
                console.log(error);
            })
    }
    

};

const unloadedState: DashboardStoreState = {
    isloading: false,
    registros: [],
    clientes: [],
    vendedores: []
};

export const reducer: Reducer<DashboardStoreState> = (state: DashboardStoreState | undefined, incomingAction: Action): DashboardStoreState => {
    if (state === undefined) {
        return unloadedState;
    }
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_INFORMACION_INICIAL_DASHBOARD':
            return {
                ...state,
                isloading: true,
                registros: []
            };
        case 'RECEIVE_INFORMACION_INICIAL_DASHBOARD':
            return {
                ...state,
                isloading: false,
                registros: action.registros
            };
        case 'RECEIVE_CLIENTES':
            return {
                ...state,
                clientes: action.clientes
            };
        case 'RECEIVE_VENDEDORES':
            return {
                ...state,
                vendedores: action.vendedores
            };
        default:
            return state;
    }
};

