﻿import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';
import { fetch, addTask } from 'domain-task';
import axios from 'axios';
import * as _ from 'lodash';
import * as url from '../common';

export interface EmbarqueDetallesStoreState {
    isloading: boolean,
    registros: any[]
}

export interface RequestInformacionInicialEmbarqueDetalle {
    type: 'REQUEST_INFORMACION_INICIAL_Embarque_DETALLES'
}

export interface ReceiveInformacionInicialEmbarqueDetalle {
    type: 'RECEIVE_INFORMACION_INICIAL_Embarque_DETALLES',
    registros: any[]
}

type KnownAction = RequestInformacionInicialEmbarqueDetalle | ReceiveInformacionInicialEmbarqueDetalle;

export const actionCreators = {
    requestInformacionInicialEmbarqueDeta: (grupo: string, cliente: string, vendedor: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({ type: 'REQUEST_INFORMACION_INICIAL_Embarque_DETALLES' });
        var data_str = {
            CardCode: cliente ? cliente : '',
            Fecha_grupo: grupo,
            SlpCode: vendedor ? vendedor : 0
        };
        console.log(data_str);
        let fetchTask = axios.post(url.EmbarqueDetalle, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                //console.log(data);
                const registros = _.filter(data.OData, (x: any) => {

                    return x.Mercado == grupo;
                });
                console.log(registros);
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_Embarque_DETALLES', registros: data.OData });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_Embarque_DETALLES', registros: [] });
                console.log(error);
            })

        addTask(fetchTask);
    },
    requestDowloadExcel: (grupo: string, cliente: string, vendedor: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            CardCode: cliente ? cliente : '',
            Fecha_grupo: grupo,
            SlpCode: vendedor ? vendedor : 0
        };
        console.log(JSON.stringify(data_str));
        var respnseA = axios.post(url.EmbarqueExcel, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            },
            responseType: 'blob'
        })
            .then(response => {
                //let archivo = response.headers["content-disposition"];
                //let tmp = archivo.split("=");
                //archivo = tmp[1];
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', "Embarques.xls");
                document.body.appendChild(link);
                link.click();
            })
            .catch(error => {
                console.log(error);
            })
    }
};

const unloadedState: EmbarqueDetallesStoreState = {
    isloading: false,
    registros: []
};

export const reducer: Reducer<EmbarqueDetallesStoreState> = (state: EmbarqueDetallesStoreState | undefined, incomingAction: Action): EmbarqueDetallesStoreState => {
    if (state === undefined) {
        return unloadedState;
    }
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_INFORMACION_INICIAL_Embarque_DETALLES':
            return {
                ...state,
                isloading: true,
                registros: []
            };
        case 'RECEIVE_INFORMACION_INICIAL_Embarque_DETALLES':
            return {
                ...state,
                isloading: false,
                registros: action.registros
            };

        default:
            return state;
    }
};

