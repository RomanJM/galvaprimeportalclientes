﻿import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';
import { fetch, addTask } from 'domain-task';
import axios from 'axios';
import * as _ from 'lodash';
import * as url from '../common';

export interface EstadoCuentaDetalleStoreState {
    isloading: boolean,
    registros: any
}

export interface RequestInformacionInicialEstadoCuentaDetalle {
    type: 'REQUEST_INFORMACION_INICIAL_EstadoCuenta_DETALLES'
}

export interface ReceiveInformacionInicialEstadoCuentaDetalle {
    type: 'RECEIVE_INFORMACION_INICIAL_EstadoCuenta_DETALLES',
    registros: any
}

type KnownAction = RequestInformacionInicialEstadoCuentaDetalle | ReceiveInformacionInicialEstadoCuentaDetalle;

export const actionCreators = {
    requestInformacionInicialEstadoCuentaDeta: (cliente: string,vendedor : number, periodo : string): AppThunkAction<KnownAction> => (dispatch, getState) => {
           var data_str = {
               CardCode: cliente ? cliente : '',
            _Vence: periodo,
               SlpCode: vendedor ? vendedor: 0
        };
        dispatch({ type: 'REQUEST_INFORMACION_INICIAL_EstadoCuenta_DETALLES' });
        let fetchTask = axios.post(url.EstadoCuentaDetalle, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_EstadoCuenta_DETALLES', registros: data.OData });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_EstadoCuenta_DETALLES', registros: [] });
                console.log(error);
            })

        addTask(fetchTask);
    },
    requestDowloadExcel: (cliente: string, vendedor: number, periodo: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            CardCode: cliente ? cliente : '',
            _Vence: periodo,
            SlpCode: vendedor ? vendedor : 0
        };
        console.log("JSON."+JSON.stringify(data_str));
        var respnseA = axios.post(url.EstadoCuentaExcel, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            },
            responseType: 'blob'
        })
            .then(response => {
                //let archivo = response.headers["content-disposition"];
                //let tmp = archivo.split("=");
                //archivo = tmp[1];
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', "Estado de cuenta.xls");
                document.body.appendChild(link);
                link.click();
            })
            .catch(error => {
                console.log(error);
            })
    },
    requestDowloadArchivoFactura: (docNum: string, tipo: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            DocNum: docNum ? parseInt(docNum) : 0,
            Tipo: tipo
        };
        var respnseA = axios.post(url.FacturaArchivo, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            },
            responseType: 'blob'
        })
            .then(response => {
                /*
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.target = "_blank";
                link.setAttribute('open', docNum + (tipo == "PDF" ? ".pdf" : ".xml"));
                document.body.appendChild(link);
                link.click();
                */

                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                //link.setAttribute('download', docNum + (tipo == "PDF" ? ".pdf" : ".xml"));
                link.setAttribute('download', docNum + "." + tipo);
                document.body.appendChild(link);
                link.click();
            })
            .catch(error => {
                console.log(error);
            })
    }
};

const unloadedState: EstadoCuentaDetalleStoreState = {
    isloading: false,
    registros: []
};

export const reducer: Reducer<EstadoCuentaDetalleStoreState> = (state: EstadoCuentaDetalleStoreState | undefined, incomingAction: Action): EstadoCuentaDetalleStoreState => {
    if (state === undefined) {
        return unloadedState;
    }
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_INFORMACION_INICIAL_EstadoCuenta_DETALLES':
            return {
                ...state,
                isloading: true,
                registros: []
            };
        case 'RECEIVE_INFORMACION_INICIAL_EstadoCuenta_DETALLES':
            return {
                ...state,
                isloading: false,
                registros: action.registros
            };

        default:
            return state;
    }
};

