﻿import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';
import axios from 'axios';
import * as url from '../common';
import * as _ from 'lodash';

export interface EmbarquesStoreState {
    isloading: boolean,
    registros: any[],
    clientes: any[],
    vendedores: any[]
}
export interface RequestInformacionInicialEmbarquesAction {
    type: 'REQUEST_INFORMACION_INICIAL_Embarques'
}

export interface ReceiveInformacionInicialEmbarquesAction {
    type: 'RECEIVE_INFORMACION_INICIAL_Embarques',
    registros: any[]
}

export interface ReceiveInformacionClientesEmbarquesAction {
    type: 'RECEIVE_CLIENTES_Embarques',
    clientes: any[]
}

export interface ReceiveInformacionVendedoresEmbarquesAction {
    type: 'RECEIVE_VENDEDORES_Embarques',
    vendedores: any[]
}
type KnownAction = RequestInformacionInicialEmbarquesAction |
    ReceiveInformacionInicialEmbarquesAction | ReceiveInformacionClientesEmbarquesAction
    | ReceiveInformacionVendedoresEmbarquesAction;

const unloadedState: EmbarquesStoreState = {
    isloading: false,
    registros: [],
    clientes: [],
    vendedores: []
};

export const actionCreators = {
    requestInformacionInicial: (cliente: any, vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({ type: 'REQUEST_INFORMACION_INICIAL_Embarques' });
        var data_str = {
            CardCode: cliente && cliente != "0"? cliente : '',
            SlpCode: vendedor ? vendedor : 0
        };
        console.log(data_str);
        axios.post(url.EmbarqueResumen, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                console.log(data);
                console.log(data.Message2);
                var fecha: any = document.getElementById('fecha_actualizacion');
                fecha.innerHTML = data.Message2;

                var registro = _.orderBy(data.OData, (x: any) => { return x.Orden });
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_Embarques', registros: registro });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_Embarques', registros: [] });
                console.log(error);
            })
    },
    requestClientes: (vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var clientes: any = [];
        axios.post(url.ClientesGet, { SlpCode: vendedor ? vendedor : 0}, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    clientes = data.OData.map((x: any) => {
                        if (x.EsActivo)
                            return { "clave": x.CardCode, "descripcion": x.Name, "rfc": x.Rfc }
                    });
                    clientes = _.orderBy(clientes, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_CLIENTES_Embarques', clientes: data.Type == "success" ? clientes : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_CLIENTES_Embarques', clientes: [] });
                console.log(error);
            })

    },
    requestFiltraClientes: (vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            SlpCode: parseInt(vendedor)
        };
        var clientes: any = [];
        axios.post(url.ClientesGet, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    clientes = data.OData.map((x: any) => {
                        if (x.EsActivo)
                            return { "clave": x.CardCode, "descripcion": x.Name, "rfc": x.Rfc }
                    });
                    clientes = _.orderBy(clientes, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_CLIENTES_Embarques', clientes: data.Type == "success" ? clientes : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_CLIENTES_Embarques', clientes: [] });
                console.log(error);
            })

    },
    requestVendedores: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var vendedores: any = [];
        axios.post(url.VendedorGet, {}, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    vendedores = data.OData.map((x: any) => {
                        return { "clave": x.SlpCode, "descripcion": x.SlpName }
                    });
                    vendedores = _.orderBy(vendedores, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_VENDEDORES_Embarques', vendedores: data.Type == "success" ? vendedores : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_VENDEDORES_Embarques', vendedores: [] });
                console.log(error);
            })
    },
    requestDowloadExcel: (cliente: any, vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            CardCode: cliente ? cliente.value : '',
            _Vence: '',
            SlpCode: vendedor ? vendedor.value : 0
        };
        console.log("url:"+url.EmbarqueExcel);
        console.log(JSON.stringify(data_str));
        var respnseA = axios.post(url.EmbarqueExcel, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            },
            responseType: 'blob'
        })
            .then(response => {
                //let archivo = response.headers["content-disposition"];
                //let tmp = archivo.split("=");
                //archivo = tmp[1];
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', "Embarques.xls");
                document.body.appendChild(link);
                link.click();
            })
            .catch(error => {
                console.log(error);
            })
    }
};

export const reducer: Reducer<EmbarquesStoreState> = (state: EmbarquesStoreState | undefined, incomingAction: Action): EmbarquesStoreState => {
    if (state === undefined) {
        return unloadedState;
    }
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_INFORMACION_INICIAL_Embarques':
            return {
                ...state,
                isloading: true,
                registros: []
            };
        case 'RECEIVE_INFORMACION_INICIAL_Embarques':
            return {
                ...state,
                isloading: false,
                registros: action.registros
            };
        case 'RECEIVE_CLIENTES_Embarques':
            return {
                ...state,
                clientes: action.clientes
            };
        case 'RECEIVE_VENDEDORES_Embarques':
            return {
                ...state,
                vendedores: action.vendedores
            };
        default:
            return state;
    }
};