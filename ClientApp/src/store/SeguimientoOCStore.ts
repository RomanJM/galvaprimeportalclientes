﻿import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';
import axios from 'axios';
import * as url from '../common';
import * as _ from 'lodash';

export interface SeguimientoOCStoreState {
    isloading: boolean,
    registros: any[],
    clientes: any[],
    vendedores : any []
}

export interface RequestInformacionInicial {
    type: 'REQUEST_INFORMACION_INICIAL'
}

export interface ReceiveInformacionInicial {
    type: 'RECEIVE_INFORMACION_INICIAL',
    registros: any[]
}
export interface ReceiveInformacionClientes {
    type: 'RECEIVE_CLIENTES',
    clientes: any[]
}
export interface ReceiveInformacionVendedores {
    type: 'RECEIVE_VENDEDORES',
    vendedores: any[]
}

type KnownAction = RequestInformacionInicial | ReceiveInformacionInicial | ReceiveInformacionClientes | ReceiveInformacionVendedores;

export const actionCreators = {
    requestInformacionInicial: (cliente: any, vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            CardCode: cliente && cliente != "0" ? cliente : '',
            SlpCode: vendedor ? vendedor: 0
        };
        dispatch({ type: 'REQUEST_INFORMACION_INICIAL' });
        var respnseA = axios.post(url.SeguimientoResumen, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                console.log(data.Message2);
                var fecha: any = document.getElementById('fecha_actualizacion');
                fecha.innerHTML = data.Message2;
                var registro = _.orderBy(data.OData, (x: any) => {return x.Orden});
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL', registros: registro });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL', registros: [] });
                console.log(error);
            })
    },
    requestDowloadExcel: (cliente: string, vendedor: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            CardCode: cliente ? cliente : '',
            Vencimiento: '',
            SlpCode: vendedor ? vendedor : 0
        };
        console.log(JSON.stringify(data_str));
        var respnseA = axios.post(url.SeguimientoExcel, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            },
            responseType: 'blob'
        })
            .then(response => {
                //console.log(JSON.stringify(response.headers));
                //let archivo = response.headers["content-disposition"];
                //let tmp = archivo.split("=");
                //archivo = tmp[1];
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', (cliente ? (cliente + "_") : '') + "Seguimiento orden de compra.xls");
                document.body.appendChild(link);
                link.click();
            })
            .catch(error => {
                console.log(error);
            })

        //window.location.href = url.SeguimientoExcel;
    },
    requestClientes: (vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var clientes: any = [];
        axios.post(url.ClientesGet, { SlpCode: vendedor ? vendedor: 0 }, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                     clientes = data.OData.map((x: any) => {
                        if (x.EsActivo)
                            return { "clave": x.CardCode, "descripcion": x.Name, "rfc": x.Rfc }
                     });
                    clientes = _.orderBy(clientes, ['descripcion']);
                } 
                dispatch({ type: 'RECEIVE_CLIENTES', clientes: data.Type == "success" ? clientes : [] });
              })
            .catch(error => {
                dispatch({ type: 'RECEIVE_CLIENTES', clientes: [] });
                console.log(error);
            })
      
    },
    requestFiltraClientes: (vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            SlpCode: parseInt(vendedor)
        };
        var clientes: any = [];
        axios.post(url.ClientesGet, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    clientes = data.OData.map((x: any) => {
                        if (x.EsActivo)
                            return { "clave": x.CardCode, "descripcion": x.Name, "rfc": x.Rfc }
                    });
                    clientes = _.orderBy(clientes, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_CLIENTES', clientes: data.Type == "success" ? clientes : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_CLIENTES', clientes: [] });
                console.log(error);
            })

    },
    requestVendedores: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var vendedores: any = [];
        axios.post(url.VendedorGet, {}, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    vendedores = data.OData.map((x: any) => {                      
                        return { "clave": x.SlpCode, "descripcion": x.SlpName}
                    });
                    vendedores = _.orderBy(vendedores, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_VENDEDORES', vendedores: data.Type == "success" ? vendedores : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_VENDEDORES', vendedores: [] });
                console.log(error);
            })
    }
};

const unloadedState: SeguimientoOCStoreState = {
    isloading: false,
    registros: [],
    clientes: [],
    vendedores :[]
};

export const reducer: Reducer<SeguimientoOCStoreState> = (state: SeguimientoOCStoreState | undefined, incomingAction: Action): SeguimientoOCStoreState => {
    if (state === undefined) {
        return unloadedState;
    }
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_INFORMACION_INICIAL':
            return {
                ...state,
                isloading: true,
                registros: []
            };
        case 'RECEIVE_INFORMACION_INICIAL':
            return {
                ...state,
                isloading: false,
                registros: action.registros
            };
        case 'RECEIVE_CLIENTES':
            return {
                ...state,
                clientes: action.clientes
            };
        case 'RECEIVE_VENDEDORES':
            return {
                ...state,
                vendedores: action.vendedores
            };
        default:
            return state;
    }
};
