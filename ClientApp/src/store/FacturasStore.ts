﻿import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';
import axios from 'axios';
import * as url from '../common';
import * as _ from 'lodash';

export interface FacturasStoreState {
    isloading: boolean,
    registros: any[],
    clientes: any[],
    vendedores: any[],
    cliente :  any
}

export interface RequestInformacionInicial {
    type: 'REQUEST_INFORMACION_INICIAL_FACTURAS'
}

export interface ReceiveInformacionInicial {
    type: 'RECEIVE_INFORMACION_INICIAL_FACTURAS',
    registros: any[],
    cliente : any
}
export interface ReceiveInformacionClientes {
    type: 'RECEIVE_CLIENTES_FACTURAS',
    clientes: any[]
}
export interface ReceiveInformacionVendedores {
    type: 'RECEIVE_VENDEDORES_FACTURAS',
    vendedores: any[]
}

type KnownAction = RequestInformacionInicial | ReceiveInformacionInicial | ReceiveInformacionClientes | ReceiveInformacionVendedores;

export const actionCreators = {
    requestInformacionInicial: (cliente: any, vendedor: any, fechaInicial : any , fechaFinal : any, factura : any,remision:any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var fac = parseInt(factura);
        var rem = parseInt(remision);
        cliente = cliente && cliente != "0" ? cliente : '';
        var data_str = {
            CardCode: cliente,
            SlpCode: vendedor ? vendedor : 0,
            _Fecha_desde: fechaInicial,
            _Fecha_hasta: fechaFinal,
            DocNum: fac ? fac : 0,
            DocNumEnt: rem ? rem : 0
        };
        console.log(JSON.stringify(data_str));

        let _fecha_hasta_ = new Date(fechaFinal);
        let _fecha_desde_ = new Date(fechaInicial);
        let _total_meses_;
        _total_meses_ = (_fecha_hasta_.getFullYear() - _fecha_desde_.getFullYear()) * 12;
        _total_meses_ -= _fecha_desde_.getMonth();
        _total_meses_ += _fecha_hasta_.getMonth();

        _total_meses_++;

        
        if (_total_meses_ < 0) {
            alert('La fecha inicial debe ser anterior a la fecha final');
            return;
        }

        if (_total_meses_ > 6 && cliente != '') {
            alert('El rango de fechas para consulta con selección de cliente puede ser hasta 6 meses');
            return;
        }else if (_total_meses_ > 3 && cliente =='') {
            alert('El rango de fechas sin seleccionar un cliente puede ser hasta 3 meses');
            return;
        }

        dispatch({ type: 'REQUEST_INFORMACION_INICIAL_FACTURAS' });
        var respnseA = axios.post(url.FacturaLista, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                console.log("fecha actualiza:"+data.Message2);
                /*var localstorage = window.localStorage;
                localstorage.removeItem("FechaActualizacion");
                localstorage.setItem("FechaActualizacion", data.Message2);*/

                var fecha: any = document.getElementById('fecha_actualizacion');
                fecha.innerHTML = data.Message2;

                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_FACTURAS', registros: data.OData, cliente: cliente ? cliente : '' });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_FACTURAS', registros: [], cliente: cliente ? cliente : ''});
                console.log(error);
            })
    },
    requestDowloadExcel: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        window.location.href = url.SeguimientoExcel;
    },
    requestClientes: (vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var clientes: any = [];
        axios.post(url.ClientesGet, { SlpCode: vendedor ? vendedor : 0 }, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    clientes = data.OData.map((x: any) => {
                        if (x.EsActivo)
                            return { "clave": x.CardCode, "descripcion": x.Name, "rfc": x.Rfc }
                    });
                    clientes = _.orderBy(clientes, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_CLIENTES_FACTURAS', clientes: data.Type == "success" ? clientes : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_CLIENTES_FACTURAS', clientes: [] });
                console.log(error);
            })

    },
    requestFiltraClientes: (vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            SlpCode: parseInt(vendedor)
        };
        var clientes: any = [];
        axios.post(url.ClientesGet, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    clientes = data.OData.map((x: any) => {
                        if (x.EsActivo)
                            return { "clave": x.CardCode, "descripcion": x.Name, "rfc": x.Rfc }
                    });
                    clientes = _.orderBy(clientes, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_CLIENTES_FACTURAS', clientes: data.Type == "success" ? clientes : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_CLIENTES_FACTURAS', clientes: [] });
                console.log(error);
            })

    },
    requestVendedores: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var vendedores: any = [];
        axios.post(url.VendedorGet, {}, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    vendedores = data.OData.map((x: any) => {
                        return { "clave": x.SlpCode, "descripcion": x.SlpName }
                    });
                    vendedores = _.orderBy(vendedores, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_VENDEDORES_FACTURAS', vendedores: data.Type == "success" ? vendedores : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_VENDEDORES_FACTURAS', vendedores: [] });
                console.log(error);
            })
    },
    requestDowloadArchivo: (docNum: string, tipo: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            DocNum: docNum ? parseInt(docNum) : 0,
            Tipo : tipo
        };
        var respnseA = axios.post(url.FacturaArchivo, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            },
            responseType: 'blob'
        })
            .then(response => {
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', docNum + (tipo == "PDF" ? ".pdf" : ".xml"));
                document.body.appendChild(link);
                link.click();
            })
            .catch(error => {
                console.log(error);
            })
    },
    requestDowloadArchivoRemision: (docNum: string, tipo: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            DocNum: docNum ? parseInt(docNum) : 0,
            Tipo: tipo
        };

        console.log(url.RemisionArchivo);
        console.log(JSON.stringify(data_str));

        var respnseA = axios.post(url.RemisionArchivo, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            },
            responseType: 'blob'
        })
            .then(response => {
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', docNum + (tipo == "PDF" ? ".pdf" : ".xml"));
                document.body.appendChild(link);
                link.click();
            })
            .catch(error => {
                console.log(error);
            })
    },
    requestDowloadArchivoRecibo: (recibo: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            DocNum: recibo ? parseInt(recibo) : 0        
        };
        console.log(url.ReciboArchivo);
        console.log(JSON.stringify(data_str));

        var respnseA = axios.post(url.ReciboArchivo, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            },
            responseType: 'blob'
        })
            .then(response => {
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', recibo + ".pdf");
                document.body.appendChild(link);
                link.click();
            })
            .catch(error => {
                console.log(error);
            })
    },
    requestDowloadArchivoCertificado: (certificado: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            DocNum: certificado ? parseInt(certificado) : 0
        };
        console.log(data_str);
        var respnseA = axios.post(url.CertificadoArchivo, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            },
            responseType: 'blob'
        })
            .then(response => {
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', certificado + ".pdf");
                document.body.appendChild(link);
                link.click();
            })
            .catch(error => {
                console.log(error);
            })
    }
};

const unloadedState: FacturasStoreState = {
    isloading: false,
    registros: [],
    clientes: [],
    vendedores: [],
    cliente : ""
};

export const reducer: Reducer<FacturasStoreState> = (state: FacturasStoreState | undefined, incomingAction: Action): FacturasStoreState => {
    if (state === undefined) {
        return unloadedState;
    }
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_INFORMACION_INICIAL_FACTURAS':
            return {
                ...state,
                isloading: true,
                registros: []
            };
        case 'RECEIVE_INFORMACION_INICIAL_FACTURAS':
            return {
                ...state,
                isloading: false,
                registros: action.registros,
                cliente : action.cliente
            };
        case 'RECEIVE_CLIENTES_FACTURAS':
            return {
                ...state,
                clientes: action.clientes
            };
        case 'RECEIVE_VENDEDORES_FACTURAS':
            return {
                ...state,
                vendedores: action.vendedores
            };
        default:
            return state;
    }
};
