﻿import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';
import axios from 'axios';
import * as url from '../common';
import * as _ from 'lodash';

export interface EstadoCuentaStoreState {
    isloading: boolean,
    registros: any[],
    clientes: any[],
    vendedores: any[],
    cliente: any,
    vendedor : any
}
export interface RequestInformacionInicialEstadoCuentaAction {
    type: 'REQUEST_INFORMACION_INICIAL_ESTADOCUENTA'
}

export interface ReceiveInformacionInicialEstadoCuentaAction {
    type: 'RECEIVE_INFORMACION_INICIAL_ESTADOCUENTA',
    registros: any[],
    cliente: any,
    vendedor : any
}

export interface ReceiveInformacionClientesEstadoCuentaAction {
    type: 'RECEIVE_CLIENTES_ESTADOCUENTA',
    clientes: any[]
}

export interface ReceiveInformacionVendedoresEstadoCuentaAction {
    type: 'RECEIVE_VENDEDORES_ESTADOCUENTA',
    vendedores: any[]
}
type KnownAction = RequestInformacionInicialEstadoCuentaAction |
    ReceiveInformacionInicialEstadoCuentaAction | ReceiveInformacionClientesEstadoCuentaAction
    | ReceiveInformacionVendedoresEstadoCuentaAction;

const unloadedState: EstadoCuentaStoreState = {
    isloading: false,
    registros: [],
    clientes: [],
    vendedores: [],
    cliente: "",
    vendedor : 0
};

export const actionCreators = {
    requestInformacionInicial: (cliente: any, vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({ type: 'REQUEST_INFORMACION_INICIAL_ESTADOCUENTA' });
        var data_str = {
            CardCode: cliente && cliente != "0" ? cliente : '',
            SlpCode: vendedor ? vendedor : 0
        };
        axios.post(url.EstadoCuentaResumen, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                var fecha: any = document.getElementById('fecha_actualizacion');
                fecha.innerHTML = data.Message2;
                var registro = _.orderBy(data.OData, (x: any) => { return x.Orden });
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_ESTADOCUENTA', registros: registro, cliente : cliente, vendedor : vendedor });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_ESTADOCUENTA', registros: [] , cliente: cliente, vendedor : vendedor});
                console.log(error);
            })
    },
    requestClientes: (vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var clientes: any = [];
        axios.post(url.ClientesGet, { SlpCode: vendedor ? vendedor : 0 }, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    clientes = data.OData.map((x: any) => {
                        if (x.EsActivo)
                            return { "clave": x.CardCode, "descripcion": x.Name, "rfc": x.Rfc }
                    });
                    clientes = _.orderBy(clientes, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_CLIENTES_ESTADOCUENTA', clientes: data.Type == "success" ? clientes : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_CLIENTES_ESTADOCUENTA', clientes: [] });
                console.log(error);
            })

    },
    requestFiltraClientes: (vendedor : any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            SlpCode: parseInt(vendedor)
        };
        var clientes: any = [];
        axios.post(url.ClientesGet, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    clientes = data.OData.map((x: any) => {
                        if (x.EsActivo)
                            return { "clave": x.CardCode, "descripcion": x.Name, "rfc": x.Rfc }
                    });
                    clientes = _.orderBy(clientes, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_CLIENTES_ESTADOCUENTA', clientes: data.Type == "success" ? clientes : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_CLIENTES_ESTADOCUENTA', clientes: [] });
                console.log(error);
            })

    },
    requestVendedores: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var vendedores: any = [];
        axios.post(url.VendedorGet, {}, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                if (data.Type == "success") {
                    vendedores = data.OData.map((x: any) => {
                        return { "clave": x.SlpCode, "descripcion": x.SlpName }
                    });
                    vendedores = _.orderBy(vendedores, ['descripcion']);
                }
                dispatch({ type: 'RECEIVE_VENDEDORES_ESTADOCUENTA', vendedores: data.Type == "success" ? vendedores : [] });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_VENDEDORES_ESTADOCUENTA', vendedores: [] });
                console.log(error);
            })
    },
    requestDowloadExcel: (cliente: any, vendedor: any): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            CardCode: cliente ? cliente : '',
            _Vence: '',
            SlpCode: vendedor ? vendedor : 0
        };
        console.log(JSON.stringify(data_str));
        var respnseA = axios.post(url.EstadoCuentaExcel, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            },
            responseType: 'blob'
        })
            .then(response => {
                //let archivo = response.headers["content-disposition"];
                //let tmp = archivo.split("=");
                //archivo = tmp[1];
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', "Estado de cuenta.xls");
                document.body.appendChild(link);
                link.click();
            })
            .catch(error => {
                console.log(error);
            })
    }
};

export const reducer: Reducer<EstadoCuentaStoreState> = (state: EstadoCuentaStoreState | undefined, incomingAction: Action): EstadoCuentaStoreState => {
    if (state === undefined) {
        return unloadedState;
    }
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_INFORMACION_INICIAL_ESTADOCUENTA':
            return {
                ...state,
                isloading: true,
                registros: []
            };
        case 'RECEIVE_INFORMACION_INICIAL_ESTADOCUENTA':
            return {
                ...state,
                isloading: false,
                registros: action.registros,
                cliente: action.cliente,
                vendedor: action.vendedor
            };
        case 'RECEIVE_CLIENTES_ESTADOCUENTA':
            return {
                ...state,
                clientes: action.clientes
            };
        case 'RECEIVE_VENDEDORES_ESTADOCUENTA':
            return {
                ...state,
                vendedores: action.vendedores
            };
        default:
            return state;
    }
};