﻿import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';
import axios from 'axios';
import * as _ from 'lodash';
import * as url from '../common';

export interface ContactosStoreState {
    isloading: boolean,
    registrosEmail: any,
    registrosTelefono :  any
}

export interface ReceiveInformacionInicialEmail {
    type: 'RECEIVE_INFORMACION_INICIAL_CONTACTOS_EMAIL',
    registros: any
}
export interface ReceiveInformacionInicialTelefono {
    type: 'RECEIVE_INFORMACION_INICIAL_CONTACTOS_TELEFONO',
    registros: any
}


type KnownAction = ReceiveInformacionInicialTelefono | ReceiveInformacionInicialEmail;
export const actionCreators = {
    requestInformacionInicialEmail: (cliente: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
    
        var data_str = {
            CardCode: cliente && cliente != "0" ? cliente : ''
        };
        axios.post(url.ContactosEmail, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                console.log(data);
                //var registros = _.filter(data.OData, { Vencimiento: vencimiento });        
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_CONTACTOS_EMAIL', registros: data });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_CONTACTOS_EMAIL', registros: [] });
                console.log(error);
            })
    },
    requestInformacionInicialTelefono: (cliente: string): AppThunkAction<KnownAction> => (dispatch, getState) => {

        var data_str = {
            CardCode: cliente && cliente != "0" ? cliente : ''
        };
        axios.post(url.ContactosTelefono, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                console.log(data);
                //var registros = _.filter(data.OData, { Vencimiento: vencimiento });        
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_CONTACTOS_TELEFONO', registros: data });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL_CONTACTOS_TELEFONO', registros: [] });
                console.log(error);
            })
    }


};

const unloadedState: ContactosStoreState = {
    isloading: false,
    registrosEmail: [],
    registrosTelefono : []
};

export const reducer: Reducer<ContactosStoreState> = (state: ContactosStoreState | undefined, incomingAction: Action): ContactosStoreState => {
    if (state === undefined) {
        return unloadedState;
    }
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'RECEIVE_INFORMACION_INICIAL_CONTACTOS_EMAIL':
            return {
                ...state,
                isloading: false,
                registrosEmail: action.registros
            };
        case 'RECEIVE_INFORMACION_INICIAL_CONTACTOS_TELEFONO':
            return {
                ...state,
                isloading: false,
                registrosTelefono: action.registros
            };
        default:
            return state;
    }
};

