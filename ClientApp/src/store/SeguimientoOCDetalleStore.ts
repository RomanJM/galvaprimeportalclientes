﻿import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';
import axios from 'axios';
import * as _ from 'lodash';
import * as url from '../common';

export interface SeguimientoOCDetalleStoreState {
    isloading: boolean,
    registros: any[]

}

export interface RequestInformacionInicial {
    type: 'REQUEST_INFORMACION_INICIAL'
}

export interface ReceiveInformacionInicial {
    type: 'RECEIVE_INFORMACION_INICIAL',
    registros: any[]
}

type KnownAction = RequestInformacionInicial | ReceiveInformacionInicial ;

export const actionCreators = {
    requestInformacionInicial: (vencimiento: string, cliente : string, vendedor: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({ type: 'REQUEST_INFORMACION_INICIAL' });
        var data_str = {
            CardCode: cliente ? cliente : '',
            Vencimiento: vencimiento,
            SlpCode: vendedor ? vendedor : 0
        };
        console.log("detalle oc", data_str);
        var respnseA = axios.post(url.SeguimientoDetalle, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.data)
            .then(data => {
                //console.log(data);
                //var registros = _.filter(data.OData, { Vencimiento: vencimiento });        
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL', registros: data.OData });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL', registros: [] });
                console.log(error);
            })
    },
    requestDowloadExcel: (vencimiento: string, cliente: string, vendedor: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        var data_str = {
            CardCode: cliente ? cliente : '',
            Vencimiento: vencimiento,
            SlpCode: vendedor ? vendedor : 0
        };
        console.log(JSON.stringify(data_str));
        var respnseA = axios.post(url.SeguimientoExcel, data_str, {
            headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("Token"),
                'Content-Type': 'application/json'
            },
            responseType: 'blob'
        })
            .then(response => {
                //let archivo = response.headers["content-disposition"];
                //let tmp = archivo.split("=");
                //archivo = tmp[1];
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', (cliente ? (cliente+"_") : '')+"Seguimiento orden de compra.xls");
                document.body.appendChild(link);
                link.click();
            })
            .catch(error => {
                console.log(error);
            })

        //window.location.href = url.SeguimientoExcel;
    }
   
};

const unloadedState: SeguimientoOCDetalleStoreState = {
    isloading: false,
    registros: []
   
};

export const reducer: Reducer<SeguimientoOCDetalleStoreState> = (state: SeguimientoOCDetalleStoreState | undefined, incomingAction: Action): SeguimientoOCDetalleStoreState => {
    if (state === undefined) {
        return unloadedState;
    }
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_INFORMACION_INICIAL':
            return {
                ...state,
                isloading: true,
                registros: []
            };
        case 'RECEIVE_INFORMACION_INICIAL':
            return {
                ...state,
                isloading: false,
                registros: action.registros
            };
      
        default:
            return state;
    }
};

