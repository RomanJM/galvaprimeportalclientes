import * as React from 'react';

import Dashboard from './pages/Dashboard';
import { RouteComponentProps } from 'react-router';

class Home extends React.Component<RouteComponentProps<{}>, any>{
    constructor(props: any) {
        super(props);
       
    }
    componentDidMount() {
        var title: any = document.getElementById('title-page');
        title.innerHTML = "";
    }
    public render() {
        return <div className="text-center">
            <h1>Bienvenido a nuestro portal de clientes</h1>
            <div>
                <Dashboard {...this.props} />
            </div>
        </div>;
    }
}
export default Home;