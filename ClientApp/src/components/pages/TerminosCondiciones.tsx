﻿import * as React from 'react';

export default class Conozcanos extends React.Component<{}, {}>{
    componentDidMount() {
        var title: any = document.getElementById('title-page');
        title.innerHTML = "Términos y condiciones";
    }
    public render() {
        return <div className={"card "} >
            <div className="card img-fondo-card">
                <div className="card-body">
                    <div className="row">
                        <div className="col col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div style={{ textAlign: "center" }}>
                                <h4 style={{ fontWeight: 'bold' }} className="section-main-title">GARANTÍAS GENERALES</h4>
                             </div>
                                <br />
                            <p style={{ fontSize: 17 }}>
                                <h6 style={{ color: "#0e7ec3", fontSize: 17, fontWeight: 'bold' }} >INTRODUCCIÓN</h6>
                            </p>
                            <p style={{ fontSize: 17, textAlign: 'justify' }}>
                                Las garantías generales son extendidas a los productos producidos y comercializados por <strong>GALVAPRIME</strong>.
                            </p>
                            <p style={{ fontSize: 17, textAlign: 'justify' }}>
                                Los requerimientos dimensionales, planitud, superficie, recubrimientos,
                                propiedades mecánicas, química, estarán de acuerdo a las especificaciones
                                del CLIENTE aceptadas por <strong>GALVAPRIME</strong> y descritas en la Ingeniería de producto
                                y/o consistentes con las aplicables en la norma ASTM, los requisitos más restrictivos
                                están sujetos a consulta con <strong>GALVAPRIME</strong> antes de la aceptación del pedido,
                                los atributos no explícitos se rigen por las especificaciones técnicas de <strong>GALVAPRIME</strong>.
                            </p>
                            <p style={{ fontSize: 17 }}>
                                < h6 style={{ color: "#0e7ec3", fontSize: 17, fontWeight: 'bold' }} >GARANTÍA</h6>
                            </p>
                            <p style={{ fontSize: 17, textAlign: 'justify' }}>
                                <strong>GALVAPRIME</strong>, garantiza que los productos suministrados o producidos por <strong>GALVAPRIME</strong>,
                                cumplen con las especificaciones técnicas establecidas en la Ingeniería del Producto y Fichas técnicas.
                            </p>
                            <p style={{ fontSize: 17 }}>
                                <h6 style={{ color: "#0e7ec3", fontSize: 17, fontWeight: 'bold' }} >POLÍTICA PARA ÓXIDO Y MANCHAS</h6>
                            </p>
                            <p style={{ fontSize: 17, textAlign: 'justify' }}>
                                <strong>GALVAPRIME</strong> no aceptará reclamos de óxido y/o manchas bajo las siguientes condiciones: <br/>
                                • Manejo y/o almacenamiento inadecuado en las instalaciones del CLIENTE. <br />
                                • Productos como rolado en caliente decapado, rolado en frio recocido ordenados como seco, es decir, sin aceite, incluye material ordenado como aceitado muy ligero.<br />
                                • Productos recubiertos ordenados sin protección, es decir, sin aceite o pasivación.<br />
                                • Producto rolado en frío recocido después de 30 días del envío.<br />
                                • Producto revestido con tratamiento Pasivado después de 30 días del envío.<br />
                                • Producto revestido con protección aceitado nivel medio después de 30 días del envío.<br />
                                • En maquila de Acero inoxidable y aluminio, se recomienda el uso de película protectora de polietileno con adhesivo, si el cliente solicita que al material no se aplique dicha película, <strong>GALVAPRIME</strong> no garantiza el acabado superficial o defectos inherentes al contacto entre laminas.<br />
                            </p>
                            <p style={{ fontSize: 17 }}>
                                <h6 style={{ color: "#0e7ec3", fontSize: 17, fontWeight: 'bold' }} >VARIACIONES DE PESO</h6>
                            </p>
                            <p style={{ fontSize: 17, textAlign: 'justify' }}>
                                Una variación entre el peso total real facturado por <strong>GALVAPRIME</strong> y el peso en la escala del CLIENTE de hasta el 1%, ya sea bajo o por encima, será permisible. Una diferencia fuera de este rango, será negociable. <br/>
                                Nota: Se deberá evidenciar la calibración de la báscula utilizada donde se está validando la lectura del peso.
                            </p>
                            <p style={{ fontSize: 17 }}>
                                <h6 style={{ color: "#0e7ec3", fontSize: 17, fontWeight: 'bold' }} >GOLPES</h6>
                            </p>
                            <p style={{ fontSize: 17, textAlign: 'justify' }}>
                                <strong>GALVAPRIME</strong> no será responsable por eventuales daños causados durante la maniobra de descarga, almacenaje y manejo del producto y/o uso en los procesos del CLIENTE, en algún proceso de reclamo de este tipo, el CLIENTE, deberá tener evidencia necesaria que permita concluir que el golpe o afectación es de origen.
                            </p>
                            <p style={{ fontSize: 17 }}>
                                <h6 style={{ color: "#0e7ec3", fontSize: 17, fontWeight: 'bold' }} >EXPIRACIÓN DE GARANTÍAS</h6>
                            </p>
                            <p style={{ fontSize: 17, textAlign: 'justify' }}>
                                Después de un período de 120 días, el producto no podrá ser sujeto a reclamaciones por ningún concepto, por mencionar algunos, propiedades mecánicas, químicas, dimensiones, variaciones de peso, etc.
                            </p>
                        </div>
                       
                    </div>

                </div>
            </div>
        </div>;
    }
}