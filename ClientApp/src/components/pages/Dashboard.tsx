﻿import * as React from 'react';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../../store';
import * as DashboardStore from '../../store/DashboardStore';
import numeral from 'numeral';
import Select from 'react-select';
import { Button } from '@material-ui/core';
/**
 * Components
 * */

import Loading from '../common/Loading';
import { Zoom} from '@material-ui/core';
import { yellow } from '@material-ui/core/colors';

type DashboardProps =
    DashboardStore.DashboardStoreState &
    typeof DashboardStore.actionCreators &
    RouteComponentProps<{}>;
interface Dashboardtate {
    cliente: any,
    vendedor: any
}

class Dashboard extends React.Component<DashboardProps, Dashboardtate>{

    constructor(props: any) {
        super(props);
        this.state = {
            cliente: null,
            vendedor: null
        };
    }

    componentDidMount() {
        var rol = window.localStorage.getItem("rolusuario");
        var cliente: any = "";
        if (rol == "CLI") {
            cliente = window.localStorage.getItem("Usuario_id");
        }
        var vendedor: any = "";
        if (rol == "VEN") {
            vendedor = window.localStorage.getItem("ZEmpleado");
        }
        this.props.requestInformacionInicial(cliente, vendedor);
        this.props.requestClientes(vendedor);
        this.props.requestVendedores();
    }
    public filtrar = () => {
        this.props.requestInformacionInicial(this.state.cliente ? this.state.cliente.value : '', this.state.vendedor ? this.state.vendedor.value : '');
    }

    public render() {
        var totalInventario = 0;
        var totalOrdenCompra = 0;
        var totalEmbarque = 0;
        var totalEstadoCuenta = 0;

        totalInventario = _.sumBy(this.props.registros.ResumenAlmacen, (x: any) => {
            return x.Valor;
        });
        totalOrdenCompra = _.sumBy(this.props.registros.ResumenOV, (x: any) => {
            return x.Valor;
        });
        totalEmbarque = _.sumBy(this.props.registros.ResumenEmbarques, (x: any) => {
            return x.Valor;
        });
        var primerRegistro = _.find(this.props.registros.ResumenEdoCta, (item: any) => {
            return item;
        });
        var valor = primerRegistro ? primerRegistro.Valor2 : 0;
        var registrosFilter = this.props.registros ? _.filter(this.props.registros.ResumenEdoCta, (item: any) => {
            return item.ValorTxt.substr(item.ValorTxt.length - 3) === (valor == 0 || valor == 2 ? "USD" : "MXN");
        }) : [];

        totalEstadoCuenta = _.sumBy(registrosFilter, (x: any) => {
            return x.Valor;
        });
        const props = this.props;
        var rol = window.localStorage.getItem("rolusuario");
        var styilos = {
            option: (styles: any, { data, isDisabled, isFocused, isSelected }: { data: any, isDisabled: any, isFocused: any, isSelected: any }) => {
                return {
                    ...styles,
                    backgroundColor: isDisabled
                        ? null
                        : isSelected
                            ? '#0e7ec3'
                            : isFocused
                                ? '#B2D4FF'
                                : null,
                    ':active': {
                        ...styles[':active'],
                        backgroundColor: '#0e7ec3',
                    },
                };
            },
        };
        var clientes = [];
        if (this.props.clientes.length > 0)
            clientes.push({ value: "0", label: '- TODO -' });
        this.props.clientes.map((item: any) => {
            clientes.push({ value: item.clave, label: item.descripcion })
        });
        var vendedores = [];
        if (this.props.vendedores.length > 0)
            vendedores.push({ value: 0, label: '- TODO -' });
        this.props.vendedores.map((item: any) => {
            vendedores.push({ value: item.clave, label: item.descripcion })
        });

        return (
            this.props.isloading ? <Loading mensage={"Cargando informacion inicial, por favor espere..."} /> :
                <div>
                    {rol == "CLI" ? null :
                        <Zoom in={true} timeout={1500}>
                            <div className="card img-fondo-card" >
                                <div className="card-body">
                                    <div className="row" >
                                        <div className="col col-sm-12 col-xs-12 col-md-2 col-lg-2" style={{ margin: '1px 0' }} >

                                        </div>
                                        {(rol == "ADM" || rol == "JF1") ?

                                            <div className="col col-sm-12 col-xs-12 col-md-4 col-lg-4" style={{ margin: '1px 0' }} >
                                                <Select
                                                    className="react-select"
                                                    value={this.state.vendedor}
                                                    options={vendedores}
                                                    onChange={(valor: any) => {
                                                        this.setState({ vendedor: valor, cliente: null });
                                                        this.props.requestFiltraClientes(valor.value);
                                                        this.props.requestInformacionInicial("", valor ? valor.value : '');
                                                    }}
                                                    placeholder={"Seleccionar vendedor"}
                                                    defaultValue={{ value: 0, label: '- TODO -' }}
                                                    styles={styilos}
                                                />

                                            </div>
                                            : null}

                                        {(rol == "ADM" || rol == "VEN" || rol == "JF1") ?
                                            <div className="col col-sm-12 col-xs-12 col-md-4 col-lg-4" style={{ margin: '1px 0' }} >
                                                <Select
                                                    value={this.state.cliente}
                                                    options={clientes}
                                                    onChange={(valor: any) => {
                                                        this.setState({ cliente: valor });
                                                        this.props.requestInformacionInicial(valor ? valor.value : '', this.state.vendedor ? this.state.vendedor.value : '');
                                                    }}
                                                    placeholder={"Seleccionar cliente"}
                                                    defaultValue={{ value: "0", label: '- TODO -' }}
                                                    styles={styilos}
                                                />

                                            </div>
                                            : null}

                                        <div className="col col-sm-12 col-xs-12 col-md-2 col-lg-2" style={{ margin: '1px 0', textAlign: 'center' }} >
                                            <Button
                                                style={{ textTransform: "unset", backgroundColor: "#0e7ec3", color: "#FFFFFF" }}
                                                variant="contained"
                                                size="small"
                                                startIcon={<i className="fas fa-search"></i>}
                                                onClick={() => { this.filtrar() }}
                                            > Buscar
                                            </Button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Zoom>
                    }
                    <Zoom in={true} timeout={1500}>
                        <div className="card img-fondo-card" >
                            <div className="card-body">                               
                                <div >
                                    <div className="row">
                                        <div className="col col-lg-2 col-md-2 col-xs-12 col-sm-12">
                                         </div>
                                        <div className="col col-lg-4 col-md-4 col-xs-12 col-sm-12">
                                            <div className ="card text-center"  onClick={() => {
                                                props.history.push({
                                                    pathname: `/SeguimientoOC`
                                                });
                                            }} style={{ cursor: "pointer", backgroundColor: "#0e7ec3", color: "#fff", textAlign: 'center', padding: '5vh' }} >

                                                <div> {numeral(totalOrdenCompra ? totalOrdenCompra : 0).format('0,0')} KG </div>
                                                <div>  Seguimiento a Órdenes de Compra Abiertas </div>
                                                <i className="fas fa-shopping-cart fa-2x"></i>
                                            </div>
                                        
                                       </div>
                                    <div className="col col-lg-4 col-md-4 col-xs-12 col-sm-12">
                                            <div onClick={() => {
                                                props.history.push({
                                                    pathname: `/Inventarios`
                                                });
                                            }} className="card text-center" style={{ cursor: "pointer", backgroundColor: "#0e7ec3", color: "#fff", textAlign: 'center', padding: '5vh'}}  >
                                                <div>  {numeral(totalInventario ? totalInventario : 0).format('0,0')} KG</div>
                                                <div>  Inventarios de Producto Terminado y Por Empaque </div>
                                                <i className="fas fa-box-open fa-2x"></i>
                                            </div>
                                        </div>
                                        <div className="col col-lg-2 col-md-2 col-xs-12 col-sm-12">
                                        </div>
                                </div>
                                    <div className="row">
                                        <div className="col col-lg-2 col-md-2 col-xs-12 col-sm-12">
                                        </div>
                                    <div className="col col-lg-4 col-md-4 col-xs-12 col-sm-12">
                                            <div onClick={() => {
                                                props.history.push({
                                                    pathname: `/EstadoCuenta`
                                                });
                                            }} className="card text-center" style={{ cursor: "pointer", backgroundColor: "#0e7ec3", color: "#fff", height: "5%", textAlign: 'center', padding: '5vh'}}  >
                                                <div>  {numeral(totalEstadoCuenta ? totalEstadoCuenta : 0).format('$0,0.00')}</div>
                                                <div>  Estado de cuenta </div>
                                                <i className="fas fa-coins fa-2x"></i>
                                            </div>
                                    </div>
                                    <div className="col col-lg-4 col-md-4 col-xs-12 col-sm-12">
                                            <div onClick={() => {
                                                props.history.push({
                                                    pathname: `/Embarques`
                                                });
                                            }} className="card text-center" style={{ cursor: "pointer", backgroundColor: "#0e7ec3", color: "#fff", textAlign: 'center', padding:'5vh' }}  >
                                                <div>  {numeral(totalEmbarque ? totalEmbarque : 0).format('0,0')} KG</div>
                                                <div>  Historial de Embarques </div>
                                                <i className="fas fa-truck-moving fa-2x"></i>
                                            </div>
                                        </div>
                                        <div className="col col-lg-2 col-md-2 col-xs-12 col-sm-12">
                                        </div>
                                </div>
                                </div>                            
                           </div>
                        </div>
                    </Zoom>
                </div>
        );
    }
}

export default connect(
    (state: ApplicationState) => state.DashboardStore, DashboardStore.actionCreators
)(Dashboard as any);