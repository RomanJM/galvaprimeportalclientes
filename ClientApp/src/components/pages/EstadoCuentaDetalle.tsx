﻿import * as React from 'react';
import * as _ from 'lodash';
import Tabla from '../common/Tabla';
import numeral from 'numeral';
import { CardHeader, CardTitle } from 'reactstrap';
import { Button, Card, CardContent } from '@material-ui/core';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../../store';
import * as EstadoCuentaDetalleStore from '../../store/EstadoCuentaDetalleStore';
import Loading from '../common/Loading';
import moment from 'moment';

type EstadoCuentaDetalleProps =
    EstadoCuentaDetalleStore.EstadoCuentaDetalleStoreState &
    typeof EstadoCuentaDetalleStore.actionCreators &
    RouteComponentProps<{}>;

interface EstadoCuentaDetalleState {
    periodo: string,
    cliente: string,
    vendedor: any,
    catalogoClientes: any
}
class EstadoCuentaDetalle extends React.Component<EstadoCuentaDetalleProps, EstadoCuentaDetalleState>{
    constructor(props: any) {
        super(props);
        this.state = {
            periodo: "",
            cliente: "",
            vendedor: "",
            catalogoClientes: []
        };
    }
    componentDidMount() {
        var title: any = document.getElementById('title-page');
        title.innerHTML = "Historial de estado de cuenta por período";
        const state = (this.props.location as any).state;
        this.setState({ periodo: state.periodo, cliente: state.cliente, vendedor: state.vendedor, catalogoClientes: state.catalogoClientes });
        this.props.requestInformacionInicialEstadoCuentaDeta(state.cliente, state.vendedor, state.periodo);
    }

    public render() {
        const columns = [
            {
                Header: 'No. Documento',
                accessor: 'DocNum',
                Cell: (row: any) => {
                    return <span style={{ cursor: "pointer", textDecoration: row.original.Tipo == "F" ? "underline" : "none" }}
                        onClick={() => {
                            if (row.original.Tipo == "F") {
                                this.props.requestDowloadArchivoFactura(row.original.DocNum, "PDF");
                            }
                        }}
                    >{row.value}</span>;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Zip',
                accessor: 'DocNum',
                Cell: (row: any) => {
                    return <span style={{ cursor: "pointer", textDecoration: row.original.Tipo == "F" ? "underline" : "none" }}
                        onClick={() => {
                            if (row.original.Tipo == "F") {
                                this.props.requestDowloadArchivoFactura(row.original.DocNum, "zip");
                            }
                        }}
                    >{row.value}</span>;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Clase Documento',
                accessor: 'Tipo',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Fecha',
                accessor: 'DocDate',
                Cell: (row: any) => {
                    return moment(row.value).format("YY-MM-DD");
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Fecha de vencimiento',
                accessor: 'DocDueDate',
                Cell: (row: any) => {
                    return moment(row.value).format("YY-MM-DD");
                },
                headerStyle: { fontWeight: "bold" },
                minWidth: 150
            },

            {
                Header: 'Días de atraso',
                accessor: 'Vencido',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Periodo',
                accessor: 'Debit0USD',
                Cell: (row: any) => {
                    var periodo = "";
                    var color = "#FE2E2E";
                    var colorTexto = "#FFFFFF";
                    if (row.original.Debit0USD != 0) {
                        periodo = "Por vencer";
                        color = "#00B050";
                    } else if (row.original.Debit15USD != 0) {
                        periodo = "De 1 a 15";
                        color = "#FFFF00";
                        colorTexto = "#555454";
                    }
                    else if (row.original.Debit30USD != 0) {
                        periodo = "De 15 a 30";
                        color = "#FFC000";
                    }
                    else if (row.original.Debit60USD != 0) {
                        periodo = "De 30 a 60";
                        color = "#FF6600";
                    }
                    else if (row.original.Debit90USD != 0) {
                        periodo = "De 60 a 90";
                        color = "#A50021";
                    }
                    else {
                        periodo = "Mas de 91";
                        color = "#FF0000";
                    }
                    return <span style={{ backgroundColor: color, color: colorTexto }} className="label label-table label-secondary">{periodo}</span>;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Moneda',
                accessor: 'ISOCurrCod',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Monto',
                Cell: (row: any) => {
                    var valor = row.original.ISOCurrCod == "MXN" ? row.original.DocTotal : row.original.DocTotalFC;
                    return <div style={{ textAlign: "right" }}>{numeral(valor).format('0,0.00')}</div>;
                },
                headerStyle: { fontWeight: "bold" },
                minWidth: 150
            },
            {
                Header: 'Abono',
                Cell: (row: any) => {
                    var valor = row.original.ISOCurrCod == "MXN" ? row.original.PaidToDate : row.original.PaidFC;
                    return <div style={{ textAlign: "right" }}>{numeral(valor).format('0,0.00')}</div>;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Saldo',
                Cell: (row: any) => {
                    var total = row.original.ISOCurrCod == "MXN" ? row.original.DocTotal : row.original.DocTotalFC;
                    var abono = row.original.ISOCurrCod == "MXN" ? row.original.PaidToDate : row.original.PaidFC;
                    var result = total - abono;
                    return <div style={{ textAlign: "right", paddingRight: '20px' }}>{numeral(result).format('0,0.00')}</div>;
                },
                headerStyle: { fontWeight: "bold" }
            }
        ];
        if (!this.state.cliente) {
            columns.unshift(
                {
                    Header: 'Nombre cliente',
                    accessor: '_Alias',
                    Cell: (row: any) => {
                        return row.value;
                    },
                    headerStyle: { fontWeight: "bold" },
                    minWidth: 200
                });
            columns.unshift(
                {
                    Header: 'Cliente',
                    accessor: 'CardCode',
                    Cell: (row: any) => {
                        return row.value;
                    },
                    headerStyle: { fontWeight: "bold" }
                });
        }
        var cliente = _.find(this.state.catalogoClientes, (x: any) => {
            return x.clave == this.state.cliente;
        });
        return (this.props.isloading ? <Loading mensage={"Procesando por favor espere..."} /> :
            < div >
                <div className="card img-fondo-card" style={{ padding: '10px 15px 2px 15px' }}>
                    <div className="card-body">
                        <div style={{ margin: 0, padding: 2 }} className="row">
                            <div className="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div className="h6"> <span style={{ backgroundColor: "#0e7ec3", color: "#fff" }} className="label label-table label-secondary">Periodo</span> <span style={{ backgroundColor: "#555454", color: "#fff" }} className="label label-table label-secondary">{this.state.periodo ? this.state.periodo : "Todos"}</span></div>
                            </div>
                            <div className="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div className="h6"> <span style={{ backgroundColor: "#0e7ec3", color: "#fff" }} className="label label-table label-secondary">Cliente</span>   <span style={{ backgroundColor: "#555454", color: "#fff" }} className="label label-table label-secondary">{cliente ? cliente.clave + " - " + cliente.descripcion : "Todos"}</span> </div>
                            </div>
                        </div>
                        {cliente ?
                            <div>
                                <div className="row">
                                    {/*<div className="col-xs-6 col-md-6">*/}
                                    {/*    <table className="table table-bordered">*/}
                                    {/*        <tbody>*/}
                                    {/*            <tr style={{ textAlign: "center" }}>*/}
                                    {/*                        <td style={{ backgroundColor: "#0e7ec3", color: "#fff", width: "65%" }} ><div  > Linea de crédito </div> </td><td style={{ backgroundColor: "#555454", color: "#fff" }}>*/}
                                    {/*                            {numeral(this.props.registros.LineaCredito).format("0,0.00") }</td>*/}
                                    {/*            </tr>*/}
                                    {/*        </tbody>*/}
                                    {/*       </table>                              */}
                                    {/*</div>*/}
                                    <div className="col-xs-6 col-md-6">
                                        <table className="table table-bordered">
                                            <tbody>
                                                <tr >
                                                    <td style={{ backgroundColor: "#0e7ec3", color: "#fff", width: "65%" }} ><div  > Saldo </div> </td>
                                                    <td style={{ backgroundColor: "#555454", color: "#fff", textAlign: "right" }}>
                                                        ${numeral(this.props.registros.Saldo).format("0,0.00")}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div className="col-xs-6 col-md-6">
                                        <table className="table table-bordered">
                                            <tbody>
                                                <tr  >
                                                    <td style={{ backgroundColor: "#0e7ec3", color: "#fff", width: "65%" }} ><div  > Días promedio de pago </div> </td>
                                                    <td style={{ backgroundColor: "#555454", color: "#fff", textAlign: "right" }}>
                                                        {numeral(this.props.registros.DiasPromedioPago).format("0,0")}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="row">
                                    {/*<div className="col-xs-6 col-md-6">*/}
                                    {/*    <table className="table table-bordered">*/}
                                    {/*        <tbody>*/}
                                    {/*            <tr style={{ textAlign: "center" }}>*/}
                                    {/*                        <td style={{ backgroundColor: "#0e7ec3", color: "#fff", width: "65%" }} ><div > Disponible </div> </td><td style={{ backgroundColor: "#555454", color: "#fff" }}> {*/}
                                    {/*                            numeral(this.props.registros.Disponible).format("0,0.00")}</td>*/}
                                    {/*            </tr>*/}
                                    {/*        </tbody>*/}
                                    {/*    </table>*/}
                                    {/*</div>*/}
                                    <div className="col-xs-6 col-md-6">
                                        <table className="table table-bordered">
                                            <tbody>
                                                <tr  >
                                                    <td style={{ backgroundColor: "#0e7ec3", color: "#fff", width: "65%" }} ><div  > Saldo vencido </div> </td>
                                                    <td style={{ backgroundColor: "#555454", color: "#fff", textAlign: "right" }}>
                                                        ${numeral(this.props.registros.SaldoVencido).format("0,0.00")}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div className="col-xs-6 col-md-6">
                                        <table className="table table-bordered">
                                            <tbody>
                                                <tr >
                                                    <td style={{ backgroundColor: "#0e7ec3", color: "#fff", width: "65%", marginRight: 0 }} ><div  > Días de plazo </div> </td>
                                                    <td style={{ backgroundColor: "#555454", color: "#fff", textAlign: "right" }} >
                                                        {numeral(this.props.registros.DiasPlazo).format("0,0")}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </div> </div> : null}
                    </div>
                </div>
                <Card>
                    <div className="row" style={{ display: "block", paddingRight: 15, paddingTop: 5 }}>
                        <Button
                            style={{ textTransform: "unset", backgroundColor: "#555454", color: "#FFFFFF", float: 'right', marginRight: '5px' }}
                            variant="contained"
                            size="small"
                            startIcon={<i className="fas fa-file-excel fa-sm" />}
                            onClick={() => { this.props.requestDowloadExcel(this.state.cliente, this.state.vendedor, this.state.periodo) }}
                        > Excel
                        </Button>
                        <Button
                            style={{ textTransform: "unset", backgroundColor: "#555454", color: "#FFFFFF", float: 'right', marginRight: '5px' }}
                            variant="contained"
                            size="small"
                            startIcon={<i className="fas fa-reply fa-sm" />}
                            onClick={() => {
                                //window.history.go(-1);
                                this.props.history.push({
                                    pathname: `/EstadoCuenta`
                                });
                            }}
                        > Regresar
                            </Button>
                    </div>
                    <CardContent>
                        <Tabla
                            registros={this.props.registros.EdoCtaLista ? this.props.registros.EdoCtaLista : []}
                            columnas={columns}
                            filterable={true}
                        />
                    </ CardContent>
                </Card>
            </div>);
    }
}
export default connect(
    (state: ApplicationState) => state.EstadoCuentaDetalleStore, EstadoCuentaDetalleStore.actionCreators
)(EstadoCuentaDetalle as any);