﻿import * as React from 'react';
import numeral from 'numeral';
import * as _ from 'lodash';
import Tabla from '../common/Tabla';
import { Button, Card, CardContent } from '@material-ui/core';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../../store';
import * as SeguimientoOCDetalleStore from '../../store/SeguimientoOCDetalleStore';
import Loading from '../common/Loading';

type SeguimientoOCDetalleProps =
    SeguimientoOCDetalleStore.SeguimientoOCDetalleStoreState &
    typeof SeguimientoOCDetalleStore.actionCreators &
    RouteComponentProps<{}>;

interface SeguimientoOCDetalleState {
    vencimiento: string,
    cliente: string,
    vendedor: any,
    catalogoClientes?: any,
    unidadPeso: string
}
class SeguimientoOCDetalle extends React.Component<SeguimientoOCDetalleProps, SeguimientoOCDetalleState>{
    constructor(props: any) {
        super(props);
        this.state = {
            vencimiento: "",
            cliente: "",
            vendedor: "",
            catalogoClientes: [],
            unidadPeso: ""
        };
    }

    componentDidMount() {
        var title: any = document.getElementById('title-page');
        title.innerHTML = "Seguimiento a órdenes de compras abiertas - Detalles";
        const state = (this.props.location as any).state;
        this.setState({ vencimiento: state.vencimiento, cliente: state.cliente, vendedor: state.vendedor, catalogoClientes: state.catalogoClientes, unidadPeso: state.unidadPeso });
        this.props.requestInformacionInicial(state.vencimiento, state.cliente, state.vendedor);

    }

    public render() {
        var cliente = _.find(this.state.catalogoClientes, (x: any) => {
            return x.clave == this.state.cliente;
        });
        const columns = [
            {
                Header: 'Estatus',
                accessor: 'Vencimiento',
                Cell: (row: any) => {

                    return row.value;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'OC',
                accessor: 'Reference',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Num Parte',
                accessor: 'CustomerSKU',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Código',
                accessor: 'ItemCode',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Tipo Mat',
                accessor: 'MaterialType',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Grado',
                accessor: 'Grado',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Calibre',
                accessor: 'calibre',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Espesor',
                accessor: 'espesor',
                Cell: (row: any) => {
                    return numeral(row.value).format('0,0.00');
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Ancho',
                accessor: 'ancho',
                Cell: (row: any) => {
                    return numeral(row.value).format('0,0.00');
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Largo',
                accessor: 'largo',
                Cell: (row: any) => {
                    return numeral(row.value).format('0,0.00');
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'UM',
                accessor: 'UM',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Solicitado',
                accessor: '',
                Cell: (row: any) => {
                    var valor = row.original.UM == "KG" ? row.original.OrderQty : (row.original.UM == "LB" ? row.original.OrderQty * 2.20462 : row.original.OrderQtyUM);
                    return <div style={{textAlign: "right"}}>{numeral(valor).format('0,0')}</div>;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Embarcado',
                accessor: '',
                Cell: (row: any) => {
                    var valor = row.original.UM == "KG" ? row.original.DeliveredQty : (row.original.UM == "LB" ? row.original.DeliveredQty * 2.20462 : row.original.DeliveredQtyUM);
                    return <div style={{ textAlign: "right" }}>{numeral(valor).format('0,0')}</div>;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Saldo',
                accessor: '',
                Cell: (row: any) => {
                    var valor = row.original.UM == "KG" ? row.original.OpenQty : (row.original.UM == "LB" ? row.original.OpenQty * 2.20462 : row.original.OpenQtyUM);
                    return <div style={{ textAlign: "right" }}>{numeral(valor).format('0,0')}</div>;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Producto Terminado',
                accessor: 'PTstockAsigned',
                Cell: (row: any) => {
                    var valor = row.original.UM == "KG" ? row.original.PTstockAsigned : (row.original.UM == "LB" ? row.original.PTstockAsigned * 2.20462 : row.original._PTstockAsignedUM);
                    return <div style={{ textAlign: "right" }}>{numeral(valor).format('0,0')}</div>;
                    //return numeral(row.value).format('0,0');
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Por Empaque',
                accessor: 'PEstockAsigned',
                Cell: (row: any) => {
                    var valor = row.original.UM == "KG" ? row.original.PEstockAsigned : (row.original.UM == "LB" ? row.original.PEstockAsigned * 2.20462 : row.original._PEstockAsignedUM);
                    return <div style={{ textAlign: "right" }}>{numeral(valor).format('0,0')}</div>;
                    //return numeral(row.value).format('0,0');
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'En Programa',
                accessor: 'Progasig',
                Cell: (row: any) => {
                    var valor = row.original.UM == "KG" ? row.original.Progasig : (row.original.UM == "LB" ? row.original.Progasig * 2.20462 : row.original._ProgasigUM);
                    return <div style={{ textAlign: "right", paddingRight: '20px' }}>{numeral(valor).format('0,0')}</div>;
                    //return numeral(row.value).format('0,0');
                },
                headerStyle: { fontWeight: "bold" }
            }
        ];
        if (!this.state.cliente) {
            columns.unshift({
                Header: 'Cliente',
                accessor: 'alias',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" }
            })
        }
        return (this.props.isloading ? <Loading mensage={"Procesando por favor espere..."} /> :
            < div >

                <div className="card img-fondo-card" >
                    <div className="card-body">
                        <div style={{ margin: 0, padding: 2 }} className="row">
                            <div className="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div className="h6"> <span style={{ backgroundColor: "#0e7ec3", color: "#fff" }} className="label label-table label-secondary">Estatus</span> <span style={{ backgroundColor: "#555454", color: "#fff" }} className="label label-table label-secondary">{this.state.vencimiento ? this.state.vencimiento : "Todos"}</span></div>
                            </div>
                            <div className="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div className="h6"> <span style={{ backgroundColor: "#0e7ec3", color: "#fff" }} className="label label-table label-secondary">Cliente</span>   <span style={{ backgroundColor: "#555454", color: "#fff" }} className="label label-table label-secondary">{cliente ? cliente.clave + " - " + cliente.descripcion : "Todos"}</span> </div>
                            </div>
                        </div>
                        <div className="row" style={{ display: "block", paddingRight: 15, marginBottom: 10 }}>
                            {/*<Button*/}
                            {/*    id={"btn-pz"}*/}
                            {/*    style={{ textTransform: "unset", backgroundColor: "#555454", color: "#FFFFFF", marginRight: '5px', float: 'right' }}*/}
                            {/*    variant="contained"*/}
                            {/*    size="small"*/}
                            {/*    startIcon={<i className="fas fa-tape fa-sm"></i>}*/}
                            {/*    onClick={() => {*/}
                            {/*        $("#btn-lb").css({ backgroundColor: "#555454" });*/}
                            {/*        $("#btn-kg").css({ backgroundColor: "#555454" });*/}
                            {/*        $("#btn-pz").css({ backgroundColor: "#0e7ec3" });*/}
                            {/*        this.setState({ unidadPeso: "PZ" })*/}
                            {/*    }}*/}
                            {/*> PZ*/}
                            {/*            </Button>*/}
                            {/*<Button*/}
                            {/*    id={"btn-kg"}*/}
                            {/*    style={{ textTransform: "unset", backgroundColor: "#0e7ec3", color: "#FFFFFF", marginRight: '5px', float: 'right' }}*/}
                            {/*    variant="contained"*/}
                            {/*    size="small"*/}
                            {/*    startIcon={<i className="fas fa-weight fa-sm"></i>}*/}
                            {/*    onClick={() => {*/}
                            {/*        $("#btn-kg").css({ backgroundColor: "#0e7ec3" });*/}
                            {/*        $("#btn-lb").css({ backgroundColor: "#555454" });*/}
                            {/*        $("#btn-pz").css({ backgroundColor: "#555454" });*/}
                            {/*        this.setState({ unidadPeso: "KG" })*/}
                            {/*    }}*/}
                            {/*> KG*/}
                            {/*            </Button>*/}
                            {/*<Button*/}
                            {/*    id={"btn-lb"}*/}
                            {/*    style={{ textTransform: "unset", backgroundColor: "#555454", color: "#FFFFFF", marginRight: '5px', float: 'right' }}*/}
                            {/*    variant="contained"*/}
                            {/*    size="small"*/}
                            {/*    startIcon={<i className="fas fa-balance-scale-right fa-sm"></i>}*/}
                            {/*    onClick={() => {*/}
                            {/*        $("#btn-lb").css({ backgroundColor: "#0e7ec3" });*/}
                            {/*        $("#btn-kg").css({ backgroundColor: "#555454" });*/}
                            {/*        $("#btn-pz").css({ backgroundColor: "#555454" });*/}
                            {/*        this.setState({ unidadPeso: "LB" })*/}
                            {/*    }}*/}
                            {/*> LB*/}
                            {/*            </Button>*/}
                            <Button
                                style={{ textTransform: "unset", backgroundColor: "#555454", color: "#FFFFFF", float: 'right', marginRight: '5px' }}
                                variant="contained"
                                size="small"
                                startIcon={<i className="fas fa-file-excel fa-sm" />}
                                onClick={() => { this.props.requestDowloadExcel(this.state.vencimiento, this.state.cliente, this.state.vendedor) }}
                            > Excel
                            </Button>
                            <Button
                                style={{ textTransform: "unset", backgroundColor: "#555454", color: "#FFFFFF", float: 'right', marginRight: '5px' }}
                                variant="contained"
                                size="small"
                                startIcon={<i className="fas fa-reply fa-sm" />}
                                onClick={() => {
                                    window.history.go(-1);
                                    //this.props.history.push({
                                    //    pathname: `/SeguimientoOC`,
                                    //    state: {
                                    //        cliente: this.state.cliente
                                    //    }
                                    //});
                                }}
                            > Regresar
                            </Button>
                        </div>
                        <div className="row">
                            <Tabla
                                registros={this.props.registros}
                                columnas={columns}
                                filterable={true}
                            />
                        </div>
                    </div>
                </div>
            </div>);
    }
}
export default connect(
    (state: ApplicationState) => state.SeguimientoOCDetalleStore, SeguimientoOCDetalleStore.actionCreators
)(SeguimientoOCDetalle as any);