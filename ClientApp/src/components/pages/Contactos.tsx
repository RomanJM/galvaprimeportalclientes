﻿import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../../store';
import * as ContactosStore from '../../store/ContactosStore';
type ContactosProps =
    ContactosStore.ContactosStoreState &
    typeof ContactosStore.actionCreators &
    RouteComponentProps<{}>;

 class Contactos extends React.Component<ContactosProps, {}>{
    componentDidMount() {
        var title: any = document.getElementById('title-page');
        title.innerHTML = "Contactanos";
        var rol = window.localStorage.getItem("rolusuario");
        var cliente: any = "";
        if (rol == "CLI") {
            cliente = window.localStorage.getItem("Usuario_id");
            this.props.requestInformacionInicialEmail(cliente);
            this.props.requestInformacionInicialTelefono(cliente);
        }

    }
     public render() {
         console.log(this.props.registrosEmail);
         console.log(this.props.registrosTelefono);
         var rol = window.localStorage.getItem("rolusuario");
        return <div className={"card "} >
            <div className="card img-fondo-card">            
                <div className="card-body">
                    <div className="row">
                        <div className="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <h4 className="section-main-title">CONTACTO</h4>
                            <p style={{ textAlign: "justify" }}>Aquí encontrará toda la información necesaria para atenderlo como usted merece; le agradeceremos que nos contacte por este canal y a la brevedad un ejecutivo se comunicará con usted para brindarle la mejor atención y responder sus dudas.</p>
                        </div>
                        <div className="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <strong>Ubicación:</strong>
                            <ul className="list-with-icon list-unstyled">
                                <li><span ><span ><i className="fas fa-house-user" style={{ marginRight : 4 }}></i> </span></span>Calle Privada La Puerta # 2990 Interior 1 Parque Industrial La Puerta Santa Catarina, N.L. C.P.66350</li>
                                <li><span ><i className="fas fa-phone" style={{ marginRight: 4 }}></i>+52 (81) 8308 7632</span></li>
                                <li><span ><i className="fas fa-phone" style={{ marginRight: 4 }}></i>+52 (81) 8308 7633</span></li>
                                <li><span ><i className="fas fa-envelope" style={{ marginRight: 4 }}></i>contacto</span>@galvaprime.com</li>
                            </ul>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h4 className="section-main-title">NUESTRAS OFICINAS</h4>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d898.8546403189279!2d-100.49240856866466!3d25.690562227866913!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjXCsDQxJzI2LjQiTiAxMDDCsDI5JzMyLjEiVw!5e0!3m2!1ses!2smx!4v1506634648237" width="100%" height="450" frameBorder="0" style={{ border: 0, pointerEvents: "none" }} allowFullScreen></iframe>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>;
    }
}
export default connect(
    (state: ApplicationState) => state.ContactosStore, ContactosStore.actionCreators
)(Contactos as any);