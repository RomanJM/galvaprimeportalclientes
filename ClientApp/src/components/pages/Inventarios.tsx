﻿
import * as React from 'react';
import numeral from 'numeral';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../../store';
import * as InventariosStore from '../../store/InventariosStore';
import { Bar } from 'react-chartjs-2';
import Select from 'react-select';
import GraficaGral from '../common/GraficaGral';

//import 'react-select/dist/react-select.css';
/**
 * Components
 * */
import Loading from '../common/Loading';
import { Card, Collapse, IconButton, Button, FormControl, Zoom, Chip, CardActionArea, CardContent, Typography } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import { ListItem, List, ListItemSecondaryAction, ListItemText } from '@material-ui/core';
//declare const $;
type InventariosProps =
    InventariosStore.InventariosStoreState &
    typeof InventariosStore.actionCreators &
    RouteComponentProps<{}>;
interface InventariosState {
    cliente: any,
    vendedor: any,
    nombreCliente: string,
    open: boolean,
    unidadPeso: string,
    esAlmacen: boolean,
    textoDetalle: string
}

class Inventarios extends React.Component<InventariosProps, InventariosState>{

    constructor(props: any) {
        super(props);
        this.state = {
            cliente: null,
            vendedor: null,
            nombreCliente: "",
            open: true,
            unidadPeso: "KG",
            esAlmacen: true,
            textoDetalle: "Almacen"
        };
    }
    public filtrar = (esAlmacen: boolean, clienteT : any , vendedor : any) => {
        var cliente = _.find(this.props.clientes, (x: any) => {
            return x.clave == clienteT ? clienteT : "";
        });
        console.log(cliente);
        this.setState({ nombreCliente: cliente ? cliente.descripcion : "", open: true });
        if (esAlmacen)
            this.props.requestInformacionInicial(cliente ? cliente.clave : "", vendedor ? vendedor : "");
        else
            this.props.requestInformacionInicialMercado(cliente ? cliente.clave : "", vendedor ? vendedor: "");
    }
    componentDidMount() {
        var title: any = document.getElementById('title-page');
        title.innerHTML = "Inventario de producto terminado y por empaque";
        var rol = window.localStorage.getItem("rolusuario");
        var cliente: any = "";
        if (rol == "CLI") {
            cliente = window.localStorage.getItem("Usuario_id");
        }
        var vendedor: any = "";
        if (rol == "VEN") {
            vendedor = window.localStorage.getItem("ZEmpleado");
        }
        this.setState({ cliente: cliente ? { value: cliente, label: '' } : '', vendedor: vendedor ? { value: vendedor, label: '' } : '' });
        this.props.requestInformacionInicial(cliente, vendedor);
        this.props.requestClientes(vendedor);
        this.props.requestVendedores();
    }

    public render() {
        var clientes = [];
        if (this.props.clientes.length > 0)
            clientes.push({ value: "0", label: '- TODO -' });
        this.props.clientes.map((item: any) => {
            clientes.push({ value: item.clave, label: item.descripcion })
        });
        var vendedores = [];
        if (this.props.vendedores.length > 0)
            vendedores.push({ value: 0, label: '- TODO -' });
        this.props.vendedores.map((item: any) => {
            vendedores.push({ value: item.clave, label: item.descripcion })
        });
        var total = 0;
        const data = this.props.registros.map((item: any) => {
            total = total + (this.state.unidadPeso == "KG" ? item.Valor : this.state.unidadPeso == "LB" ? item.Valor2 : item.Valor3);
            return this.state.unidadPeso == "KG" ? item.Valor : this.state.unidadPeso == "LB" ? item.Valor2 : item.Valor3;
        });

        const labels = this.props.registros.map((item: any) => {
            //return this.state.esAlmacen ? item.ValorTxt : item.Grupo;
            return item.ValorTxt;
        });
        var rol = window.localStorage.getItem("rolusuario");
        const props = this.props;
        var cliente = this.state.cliente;
        var styilos = {
            option: (styles: any, { data, isDisabled, isFocused, isSelected }: { data: any, isDisabled: any, isFocused: any, isSelected: any }) => {
                return {
                    ...styles,
                    backgroundColor: isDisabled
                        ? null
                        : isSelected
                            ? '#0e7ec3'
                            : isFocused
                                ? '#B2D4FF'
                                : null,
                    ':active': {
                        ...styles[':active'],
                        backgroundColor: '#0e7ec3',
                    },
                };
            },
        };
        const options = {
            maintainAspectRatio: false	// Don't maintain w/h ratio
        }
        return (
            this.props.isloading ? <Loading mensage={"Procesando por favor espere..."} /> :
                <div>

                        <Zoom in={true} timeout={1500}>
                        <div className="card img-fondo-card" style={{ padding: '5px', margin: '0px 0px 5px 0px' }}>
                                <div className="card-body">
                                    <div className="row" >
                                    {(rol == "ADM" || rol == "JF1") ?
                                            <div className="col col-sm-12 col-xs-12 col-md-3 col-lg-3" style={{ margin: '1px 0' }} >
                                                <Select
                                                    className="react-select"
                                                    value={this.state.vendedor}
                                                    options={vendedores}
                                                    onChange={(valor: any) => {
                                                        this.setState({ vendedor: valor, cliente: null });
                                                        this.props.requestFiltraClientes(valor.value);
                                                        this.setState({ cliente: 0 });
                                                        this.filtrar(this.state.esAlmacen, "", valor ? valor.value : '');
                                                    }}
                                                    placeholder={"Seleccionar vendedor"}
                                                    defaultValue={{ value: 0, label: '- TODO -' }}
                                                    styles={styilos}
                                                />

                                            </div>
                                            : null}

                                    {(rol == "ADM" || rol == "VEN" || rol == "JF1") ?
                                            <div className="col col-sm-12 col-xs-12 col-md-3 col-lg-3" style={{ margin: '1px 0' }} >
                                                <Select
                                                    value={this.state.cliente}
                                                    options={clientes}
                                                    onChange={(valor: any) => {
                                                        this.setState({ cliente: valor });
                                                        this.filtrar(this.state.esAlmacen, valor ? valor.value : '', this.state.vendedor ? this.state.vendedor.value : '');
                                                    }}
                                                    placeholder={"Seleccionar cliente"}
                                                    defaultValue={{ value: "0", label: '- TODO -' }}
                                                    styles={styilos}
                                                />

                                            </div>
                                            : null}

                                        <div className="col col-sm-12 col-xs-12 col-md-6 col-lg-6" style={{ margin: '1px 0' }} >
                                        {this.state.esAlmacen ?
                                            <Button
                                                style={{ textTransform: "unset", backgroundColor: "#0e7ec3", color: "#fff", marginRight: '5px', float: 'right' }}
                                                variant="contained"
                                                size="small"
                                                startIcon={<i className="fas fa-shopping-cart fa-sm" />}
                                                onClick={() => {
                                                    this.setState({ esAlmacen: false, textoDetalle: "Tipo Material" });
                                                    this.filtrar(false, this.state.cliente ? this.state.cliente.value : "", this.state.vendedor ? this.state.vendedor.value : "");
                                                }}
                                            > Resúmen tipo material
                                    </Button> : null
                                        }
                                        {!this.state.esAlmacen ?
                                            <Button
                                                style={{ textTransform: "unset", backgroundColor: "#0e7ec3", color: "#fff", marginRight: '5px', float: 'right' }}
                                                variant="contained"
                                                size="small"
                                                startIcon={<i className="fas fa-store fa-sm" />}
                                                onClick={() => {
                                                    this.setState({ esAlmacen: true, textoDetalle: "Almacen" });
                                                    this.filtrar(true, this.state.cliente ? this.state.cliente.value : "", this.state.vendedor ? this.state.vendedor.value : "");
                                                }}
                                            > Resumen almacen
                                    </Button> : null
                                        }
                                       
                                        <Button
                                            id={"btn-kg"}
                                            style={{ textTransform: "unset", backgroundColor: "#0e7ec3", color: "#FFFFFF", marginRight: '5px', float: 'right' }}
                                            variant="contained"
                                            size="small"
                                            startIcon={<i className="fas fa-weight fa-sm"></i>}
                                            onClick={() => {
                                                $("#btn-kg").css({ backgroundColor: "#0e7ec3" });
                                                $("#btn-lb").css({ backgroundColor: "#555454" });
                                                $("#btn-pz").css({ backgroundColor: "#555454" });
                                                this.setState({ unidadPeso: "KG" })
                                            }}
                                        > KG
                                        </Button>
                                        <Button
                                            id={"btn-lb"}
                                            style={{ textTransform: "unset", backgroundColor: "#555454", color: "#FFFFFF", marginRight: '5px', float: 'right' }}
                                            variant="contained"
                                            size="small"
                                            startIcon={<i className="fas fa-balance-scale-right fa-sm"></i>}
                                            onClick={() => {
                                                $("#btn-lb").css({ backgroundColor: "#0e7ec3" });
                                                $("#btn-kg").css({ backgroundColor: "#555454" });
                                                $("#btn-pz").css({ backgroundColor: "#555454" });
                                                this.setState({ unidadPeso: "LB" })
                                            }}
                                        > LB
                                        </Button>
                                            <Button
                                                style={{ textTransform: "unset", backgroundColor: "#555454", color: "#fff", marginRight: '5px', float: 'right' }}
                                                variant="contained"
                                                size="small"
                                                startIcon={<i className="fas fa-file-excel fa-sm" />}
                                                onClick={() => { this.props.requestDowloadExcel(this.state.cliente, this.state.vendedor) }}
                                            > Excel
                                </Button>
                                            <Button
                                                style={{ textTransform: "unset", backgroundColor: "#0e7ec3", color: "#FFFFFF", marginRight: '5px', float: 'right' }}
                                                variant="contained"
                                                size="small"
                                                startIcon={<i className="fas fa-search"></i>}
                                            onClick={() => { this.filtrar(this.state.esAlmacen, this.state.cliente ? this.state.cliente.value : "", this.state.vendedor ?  this.state.vendedor.value : "") }}
                                            > Buscar
                                            </Button>
                              
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Zoom>
                    
                    <Zoom in={true} timeout={1500}>
                        <div className="card img-fondo-card" >
                            <div className="card-body">
                                <div style={{ display: "block", margin: 10 }} className="row">
                                    <div className="col col-sm-12 col-xs-12 col-md-9 col-lg-9">
                                        <GraficaGral
                                            label={this.state.esAlmacen ? 'Inventario de producto terminado y por empaque' : 'Inventario por tipo de material'}
                                            labels={labels}
                                            data={data}
                                            backgroundColor={"#99A3A4"}
                                            onclick={(items: any) => {
                                                if (items[0]) {
                                                    var index = items[0]._index;
                                                    const merca = props.registros[index];
                                                    var cat = props.clientes;
                                                    props.history.push({
                                                        pathname: `/InventariosDetalle`,
                                                        state: {
                                                            mercado: merca ? merca.Grupo : "",
                                                            cliente: cliente ? cliente.value : "",
                                                            vendedor: this.state.vendedor ? this.state.vendedor.value : '',
                                                            catalogoClientes: cat,
                                                            textoDetalle: this.state.textoDetalle
                                                        }
                                                    });
                                                }
                                            }}
                                        />
                                    </div>
                                    <div className="col col-sm-12 col-xs-12 col-md-3 col-lg-3">
                                        <div className="row">
                                            <div className="col-xs-12 col-md-12 col-lg-12 col-sm-12">
                                                <Card style={{ textAlign: "center" }} onClick={() => {
                                                    var estado = this.state;
                                                    props.history.push({
                                                        pathname: `/InventariosDetalle`,
                                                        state: {
                                                            vendedor: estado.vendedor ? estado.vendedor.value : "",
                                                            cliente: estado.cliente ? estado.cliente.value : "",
                                                            catalogoClientes: props.clientes,
                                                            textoDetalle: this.state.textoDetalle
                                                        }
                                                    });
                                                }}>
                                                    <CardActionArea>
                                                        <CardContent style={{ padding: 0 }}>
                                                            <Typography gutterBottom variant="h5" component="h2">
                                                                <Chip
                                                                    label={numeral(total).format("0,0")}
                                                                    color="primary"
                                                                    variant="outlined"
                                                                    style={{ fontSize: '16px', fontWeight: 'bold' }}
                                                                />
                                                            </Typography>
                                                            <Typography gutterBottom variant="h6" component="h6">
                                                                Total {this.state.unidadPeso}
                                                            </Typography>
                                                        </CardContent>
                                                    </CardActionArea>
                                                </Card>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-xs-12 col-md-12 col-lg-12 col-sm-12">
                                                <Card style={{ textAlign: "center", marginTop: 5 }} >
                                                    <CardActionArea>
                                                        <CardContent>
                                                            <List
                                                                component="nav"
                                                            >
                                                                {this.props.registros.map((x: any) => {
                                                                    return <ListItem>
                                                                        <ListItemText
                                                                            primary={this.state.esAlmacen ? x.ValorTxt : x.ValorTxt}
                                                                            secondary={this.state.unidadPeso == "KG" ? numeral(x.Valor).format("0,0") : this.state.unidadPeso == "LB" ? numeral(x.Valor2).format("0,0") : numeral(x.Valor3).format("0,0")}
                                                                        > </ListItemText>
                                                                        <ListItemSecondaryAction>
                                                                            <IconButton edge="end" aria-label="ir">
                                                                                <i onClick={() => {
                                                                                    props.history.push({
                                                                                        pathname: `/InventariosDetalle`,
                                                                                        state: {
                                                                                            vendedor: this.state.vendedor ? this.state.vendedor.value : 0,
                                                                                            cliente: this.state.cliente ? this.state.cliente.value : '',
                                                                                            catalogoClientes: this.props.clientes
                                                                                        }
                                                                                    });
                                                                                }} className="fas fa-location-arrow"></i>
                                                                            </IconButton>
                                                                        </ListItemSecondaryAction>
                                                                    </ListItem>
                                                                })}

                                                            </List>
                                                        </CardContent>
                                                    </CardActionArea>
                                                </Card>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </Zoom>
                </div>
        );
    }
}

export default connect(
    (state: ApplicationState) => state.InventariosStore, InventariosStore.actionCreators
)(Inventarios as any);