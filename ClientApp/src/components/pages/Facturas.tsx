﻿import * as React from 'react';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../../store';
import * as FacturasStore from '../../store/FacturasStore';
import Tabla from '../common/Tabla';
import Select from 'react-select';
import moment from 'moment';
/**
 * Components
 * */
import { MuiPickersUtilsProvider, KeyboardTimePicker, KeyboardDatePicker } from '@material-ui/pickers';
import Loading from '../common/Loading';
import { FormControl, IconButton, Button, InputLabel, MenuItem, Zoom, TextField, Input, InputAdornment} from '@material-ui/core';


type FacturasProps = FacturasStore.FacturasStoreState & typeof FacturasStore.actionCreators & RouteComponentProps<{}>;

interface FacturasState {
    cliente: any,
    vendedor: any,
    nombreCliente: string,
    open: boolean,
    fechaInicial: any,
    fechaFinal: any,
    Factura: any,
    Remision:any
}

class Facturas extends React.Component<FacturasProps, FacturasState>{

    constructor(props: any) {
        super(props);
        this.state = {
            cliente: null,
            vendedor: null,
            nombreCliente: "",
            open: true,
            fechaInicial: new Date(),
            fechaFinal: new Date(),
            Factura: "",
            Remision:""
        };
    }
    public filtrar = () => {
        var cliente = _.find(this.props.clientes, (x: any) => {
            return x.clave == this.state.cliente;
        });
        this.setState({ nombreCliente: cliente ? cliente.descripcion : "", open: true });
        //this.props.requestInformacionInicial(this.state.cliente, this.state.vendedor);
    }
    componentDidMount() {
        var title: any = document.getElementById('title-page');
        title.innerHTML = "Consulta de Facturas";
        var rol = window.localStorage.getItem("rolusuario");
        var cliente: any = "";
        if (rol == "CLI") {
            cliente = window.localStorage.getItem("Usuario_id");
        }
        var vendedor: any = "";
        if (rol == "VEN") {
            vendedor = window.localStorage.getItem("ZEmpleado");
        }
        this.setState({ cliente: cliente, vendedor: vendedor });
        var fechaInicio = moment(this.state.fechaInicial).format('YYYY-MM-DD');
        var fechaFin = moment(this.state.fechaFinal).format('YYYY-MM-DD');
        this.props.requestInformacionInicial(cliente, vendedor, fechaInicio, fechaFin, this.state.Factura, this.state.Remision);
        this.props.requestClientes(vendedor);
        this.props.requestVendedores();
    }
    public render() {
        var columns = [
            {
                Header: 'Fecha',
                accessor: 'DocDate',
                Cell: (row: any) => {
                    return moment(row.value).format('YYYY-MM-DD');
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Factura',
                accessor: 'DocNum',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Remisión',
                accessor: 'DocNumEnt',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" }
            }
        ];
        var colsfinal = null;
        if (this.props.cliente == '' || this.props.cliente == "0" || !this.props.cliente) {
            var col = {
                Header: 'Cliente',
                accessor: 'CardName',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" }
            };
            colsfinal = [...columns, col];
        }

        var colRem = {
            Header: 'Remisión',
            accessor: '',
            Cell: (row: any) => {
                return <div style={{ minWidth: 100 }}  >
                    <IconButton title="Descargar PDF" onClick={() => {
                        this.props.requestDowloadArchivoRemision(row.original.DocNumEnt,'PDF');
                    }} color="default" disableRipple component="span">
                        <i style={{ color: "#FE2E2E" }} className="fas fa-file-pdf fa-sm" />
                    </IconButton>
                </div>
            },
            width: 150,
            headerStyle: { fontWeight: "bold" }
        };

        var colFac = {
            Header: 'Factura',
            accessor: '',
            Cell: (row: any) => {
                return <div style={{ minWidth: 100 }}  >
                    <IconButton title = "Descargar XML" onClick={() => {
                       this.props.requestDowloadArchivo(row.original.DocNum, "XML");
                   }} color="default" disableRipple  component="span">
                       <i style={{ color: "#FFBF00"}} className="fas fa-file-invoice fa-sm" />
                    </IconButton>
                    <IconButton title="Descargar PDF"  onClick={() => {
                       this.props.requestDowloadArchivo(row.original.DocNum, "PDF");
                    }} color="default" disableRipple component="span">
                        <i style={{ color: "#FE2E2E" }} className="fas fa-file-pdf fa-sm" />
                    </IconButton>
                </div>
            },
            width: 150,
            headerStyle: { fontWeight: "bold" }
        };
        var colCer = {
            Header: 'Certificado',
            accessor: '',
            Cell: (row: any) => {
                return <IconButton title="Descargar PDF" onClick={() => {
                    this.props.requestDowloadArchivoCertificado(row.original.DocNumEnt);
                }} color="default" disableRipple component="span">
                    <i style={{ color: "#FE2E2E" }} className="fas fa-file-pdf fa-sm" />
                </IconButton>;
            },
            headerStyle: { fontWeight: "bold" }
        };
        var colRec={
            Header: 'Recibo',
            accessor: '',
            Cell: (row: any) => {
                if (row.original._Recibo > 0) {
                    return <IconButton title="Descargar PDF" onClick={() => {
                        this.props.requestDowloadArchivoRecibo(row.original.DocNumEnt);
                    }} color="default" disableRipple component="span">
                        <i style={{ color: "#FE2E2E" }} className="fas fa-file-pdf fa-sm" />
                    </IconButton>
                };
            },
            headerStyle: { fontWeight: "bold" }
        };
        colsfinal = [...columns, colFac, colRem, colCer];

        var rol = window.localStorage.getItem("rolusuario");
        if (rol != "CLI")
            colsfinal.push(colRec);

        var clientes = [];
        if (this.props.clientes.length > 0)
            clientes.push({ value: "0", label: '- TODO -' });
        this.props.clientes.map((item: any) => {
            clientes.push({ value: item.clave, label: item.descripcion })
        });
        var vendedores = [];
        if (this.props.vendedores.length > 0)
            vendedores.push({ value: 0, label: '- TODO -' });
        this.props.vendedores.map((item: any) => {
            vendedores.push({ value: item.clave, label: item.descripcion })
        });
        var styilos = {
            option: (styles: any, { data, isDisabled, isFocused, isSelected }: { data: any, isDisabled: any, isFocused: any, isSelected: any }) => {
                return {
                    ...styles,
                    backgroundColor: isDisabled
                        ? null
                        : isSelected
                            ? '#0e7ec3'
                            : isFocused
                                ? '#B2D4FF'
                                : null,
                    ':active': {
                        ...styles[':active'],
                        backgroundColor: '#0e7ec3',
                    },
                };
            },
        };
        return (
            this.props.isloading ? <Loading mensage={"Procesando por favor espere..."} /> :
                <div>

                        <Zoom in={true} timeout={1500}>
                            <div className="card img-fondo-card" >
                                <div className="card-body">
                                    <div className="row" >
                                    {(rol == "ADM" || rol == "JF1") ?
                                            <div className="col col-sm-12 col-xs-12 col-md-5 col-lg-5" style={{ margin: '1px 0' }} >
                                                <Select
                                                    className="react-select"
                                                    value={this.state.vendedor}
                                                    options={vendedores}
                                                    onChange={(valor: any) => {
                                                        this.setState({ vendedor: valor, cliente: null });
                                                        this.props.requestFiltraClientes(valor.value);
                                                    }}
                                                    placeholder={"Seleccionar vendedor"}
                                                    defaultValue={{ value: 0, label: '- TODO -' }}
                                                    styles={styilos}
                                                />

                                            </div>
                                            : null}

                                    {(rol == "ADM" || rol == "VEN" || rol == "JF1") ?
                                            <div className="col col-sm-12 col-xs-12 col-md-5 col-lg-5" style={{ margin: '1px 0' }} >
                                                <Select
                                                    value={this.state.cliente}
                                                    options={clientes}
                                                    onChange={(valor: any) => {
                                                        this.setState({ cliente: valor })
                                                    }}
                                                    placeholder={"Seleccionar cliente"}
                                                    defaultValue={{ value: "0", label: '- TODO -' }}
                                                    styles={styilos}
                                                />
                                            </div>
                                            : null}

                                        <div className="col col-sm-12 col-xs-12 col-md-2 col-lg-2" style={{ margin: '1px 0' }} >
                                            <Button
                                                style={{ textTransform: "unset", backgroundColor: "#0e7ec3", color: "#FFFFFF" }}
                                                variant="contained"
                                                size="small"
                                                startIcon={<i className="fas fa-search"></i>}
                                                onClick={() => {
                                                    var fechaInicio = moment(this.state.fechaInicial).format('YYYY-MM-DD');
                                                    var fechaFin = moment(this.state.fechaFinal).format('YYYY-MM-DD');
                                                    this.props.requestInformacionInicial(this.state.cliente ? this.state.cliente.value : "", this.state.vendedor ? this.state.vendedor.value : 0 , fechaInicio, fechaFin, this.state.Factura,this.state.Remision);
                                                }}
                                            > Buscar
                                            </Button>
                                        </div>
                                    </div>
                                    <div style={{ margin: 10, position: 'relative' }} className="row">

                                        <div className="col-xs-3 col-md-3">
                                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                                <KeyboardDatePicker
                                                    disableToolbar
                                                    format="MM/dd/yyyy"
                                                    margin="normal"
                                                    id="fecha-inicial"
                                                    label="Fecha inicial"
                                                    value={this.state.fechaInicial}
                                                    onChange={(fecha: any) => {
                                                        this.setState({ fechaInicial: fecha })
                                                    }}
                                                    KeyboardButtonProps={{
                                                        'aria-label': 'Select fecha',
                                                    }}
                                                />
                                            </MuiPickersUtilsProvider>
                                        </div>
                                        <div className="col-xs-3 col-md-3">
                                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                                <KeyboardDatePicker
                                                    disableToolbar
                                                    format="MM/dd/yyyy"
                                                    margin="normal"
                                                    id="fecha-final"
                                                    label="Fecha final"
                                                    value={this.state.fechaFinal}
                                                    onChange={(fecha: any) => {
                                                        this.setState({ fechaFinal: fecha })
                                                    }}
                                                    KeyboardButtonProps={{
                                                        'aria-label': 'Select fecha',
                                                    }}
                                                />
                                            </MuiPickersUtilsProvider>
                                        </div>
                                        <div style={{ marginTop: 12 }} className="col-xs-3 col-md-3">
                                            <FormControl className={""}>
                                                <InputLabel htmlFor="input-with-icon-adornment">Factura Número. Doc</InputLabel>
                                                <Input
                                                    id="input-with-icon-adornment"
                                                    value={this.state.Factura}
                                                    onChange={(value: any) => {
                                                        this.setState({ Factura: value.target.value })
                                                    }}
                                                    startAdornment={
                                                        <InputAdornment position="start">
                                                            <i className="fas fa-file-invoice" />
                                                        </InputAdornment>
                                                    }
                                                />
                                            </FormControl>
                                        </div>
                                        <div style={{ marginTop: 12 }} className="col-xs-3 col-md-3">
                                            <FormControl className={""}>
                                                <InputLabel htmlFor="input-with-icon-adornment">Remisión Número. Doc</InputLabel>
                                                <Input
                                                    id="input-with-icon-adornment"
                                                    value={this.state.Remision}
                                                    onChange={(value: any) => {
                                                        this.setState({ Remision: value.target.value })
                                                    }}
                                                    startAdornment={
                                                        <InputAdornment position="start">
                                                            <i className="fas fa-file-invoice" />
                                                        </InputAdornment>
                                                    }
                                                />
                                            </FormControl>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Zoom>

                    <Zoom in={true} timeout={1500}>
                        <div className="card img-fondo-card" >
                            <div className="card-body">                                                
                                <div style={{ margin: 10 }} className="row">
                                    <div className="col-xs-12 col-md-12">
                                        {this.props.registros.length > 0 ? <Tabla
                                            columnas={colsfinal}
                                            registros={this.props.registros}
                                            filterable={true}
                                        /> : <div style={{ textAlign : "center" }} > <span className="label label-table label-warning">No hay resultados para mostrar</span> </div>}
                                       
                                    </div>
                                </div>
                            </div>
                         </div>
                    </Zoom>
                </div>
        );
    }
}

export default connect(
    (state: ApplicationState) => state.FacturasStore, FacturasStore.actionCreators
)(Facturas as any);