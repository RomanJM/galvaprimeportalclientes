﻿import * as React from 'react';

export default class Conozcanos extends React.Component<{}, {}>{
    componentDidMount() {
        var title: any = document.getElementById('title-page');
        title.innerHTML = "Conózcanos";
    }
    public render() {
        return <div className={"card "} >
            <div className="card img-fondo-card">
                <div  className="card-body">
                    <div className="row">
                            <h6 className="section-main-title"><strong> LA EMPRESA</strong></h6>
                            <p style={{ textAlign: "justify", fontSize: 17 }}><strong>Galvaprime</strong> es una empresa orgullosamente mexicana dedicada a la distribución, comercialización y trasformación de materiales derivados del acero y metales con sede en la Ciudad de Santa Catarina, Nuevo León y cobertura a cualquier punto de México.</p> <br/>
                            <p style={{ textAlign: "justify", fontSize: 17 }}>Con más de 10 años de experiencia en la industria del acero, <strong>Galvaprime</strong> se ha especializado como un centro de servicio industrial, atendiendo a empresas con altas especificaciones en cortes de acero o tolerancias de alta especificación, lo que asegura una calidad en el producto terminado de nuestros clientes en el mejor tiempo de entrega, todo para facilitar sus procesos de fabricación</p><br />
                            <p style={{ textAlign: "justify", fontSize: 17 }}>De esta manera, más que un proveedor somos <strong> expertos metalúrgicos y profesionistas </strong> enfocados en brindarle productos o servicios de alta calidad, siempre con un trato amable.</p><br />
                    </div>

                    <div style={{ marginTop: 15 }} >
                        <div style={{ marginTop: 10 }} className="row">
                            <div className="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <h6 style={{ color: "#0e7ec3" }} className="section-main-title"><strong>MISIÓN</strong></h6>
                                <p style={{ textAlign: "justify", fontSize: 17 }}>Ser una empresa de servicio a la industria metal mecánica, que agrega valor a los productos y servicios que ofrecemos, siempre bajo un ambiente de negocio responsable y ético.</p> <br />
                            </div>
                            <div className="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <h6 style={{ color: "#0e7ec3" }} className="section-main-title"><strong>VISIÓN</strong></h6>
                                <p style={{ textAlign: "justify", fontSize: 17 }}>Ser la empresa preferida de nuestros socios comerciales, rentable y financieramente sana, impulsando el desarrollo del capital humano.</p> <br />
                            </div>
                        </div>
                        <div style={{ marginTop: 10 }} className="row">
                          
                            <div className="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <h6 style={{ color: "#0e7ec3" }} className="section-main-title"><strong>VALORES</strong></h6>
                                <p style={{ textAlign: "justify", fontSize: 17 }}>
                                    • Profesionalismo y Compromiso <br />
                                        • Responsabilidad y Honestidad <br />
                                        • Calidad como forma de vida <br />
                                        • Trabajo en equipo <br />
                                        • Respeto <br />
                                        • Actitud de Servicio <br />
                                </p>
                                <br />
                            </div>
                        </div>
                    </div>
                  
                </div>
            </div>
        </div>;
    }
}