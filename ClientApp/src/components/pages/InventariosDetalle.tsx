﻿import * as React from 'react';
import * as _ from 'lodash';
import Tabla from '../common/Tabla';
import numeral from 'numeral';
import { Button} from '@material-ui/core';

import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../../store';
import * as InventarioDetallesStore from '../../store/InventarioDetallesStore';
import Loading from '../common/Loading';

type InventariosDetalleProps =
    InventarioDetallesStore.InventarioDetallesStoreState &
    typeof InventarioDetallesStore.actionCreators &
    RouteComponentProps<{}>;

interface InventariosDetalleState {
    mercado: string,
    cliente: string,
    vendedor: any,
    catalogoClientes: any,
    textoDetalle: any,
    unidadPeso:any,
}
class InventariosDetalle extends React.Component<InventariosDetalleProps, InventariosDetalleState>{
    constructor(props: any) {
        super(props);
        this.state = {
            mercado: "",
            cliente: "",
            vendedor: "",
            catalogoClientes: [],
            textoDetalle: "",
            unidadPeso:"KG"
        };
    }
    componentDidMount() {
        var title: any = document.getElementById('title-page');
        title.innerHTML = "Inventario de producto - Detalles";
        const state = (this.props.location as any).state;
        this.setState({
            mercado: state.mercado,
            cliente: state.cliente,
            vendedor: state.vendedor,
            catalogoClientes: state.catalogoClientes,
            textoDetalle: state.textoDetalle
        });
        this.props.requestInformacionInicialInventarioDeta(state.mercado, state.cliente, state.vendedor);
    }
    componentWillReceiveProps(props: any) {

    }
    public render() {
        const columns = [
            {
                Header: 'Número parte cliente',
                accessor: 'Numpartecliente',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" }
            },

            {
                Header: 'Código',
                accessor: 'ArticuloSAP',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Forma',
                accessor: 'Forma',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Tipo de Material',
                accessor: 'TipodeMaterial',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Grado',
                accessor: 'Grado',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Calibre',
                accessor: 'Calibre',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Espesor',
                accessor: 'Espesor',
                Cell: (row: any) => {
                    var valor = row.original.Espesor;
                    return numeral(valor).format("0,0.00");
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Ancho',
                accessor: 'Ancho',
                Cell: (row: any) => {
                    var valor = row.original.Ancho;
                    return numeral(valor).format("0,0.00");
                },
                headerStyle: { fontWeight: "bold" }
            },        
            {
                Header: 'Largo',
                accessor: 'Largo',
                Cell: (row: any) => {
                    var valor = row.original.Largo;
                    return numeral(valor).format("0,0.00");
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Prod Term',
                accessor: 'ProdTerm',
                Cell: (row: any) => {
                    var valor = this.state.unidadPeso == "KG" ? row.original.ProdTerm : (this.state.unidadPeso == "LB" ? row.original.ProdTerm * 2.20462 : row.original._ProdTerm_Pz);                    
                    return <div style={{ textAlign: "right" }}>{numeral(valor).format('0,0')}</div>;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Por empaque',
                accessor: 'PorEmpaque',
                Cell: (row: any) => {
                    var valor = this.state.unidadPeso == "KG" ? row.original.PorEmpaque : (this.state.unidadPeso == "LB" ? row.original.PorEmpaque * 2.20462 : row.original._PorEmpaque_Pz);
                    return <div style={{ textAlign: "right" }}>{numeral(valor).format('0,0')}</div>;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Total general',
                accessor: 'TotalGeneral',
                Cell: (row: any) => {
                    var valor = this.state.unidadPeso == "KG" ? row.original.TotalGeneral : (this.state.unidadPeso == "LB" ? row.original.TotalGeneral * 2.20462 : row.original._TotalGeneral_Pz);
                    return <div style={{ textAlign: "right", paddingRight: '20px' }}>{numeral(valor).format('0,0')}</div>;
                },
                headerStyle: { fontWeight: "bold" }
            }
        ];
        if (!this.state.cliente) {
            columns.unshift({
                Header: 'Cliente',
                accessor: 'Cliente',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" }
            })
        }
        var cliente = _.find(this.state.catalogoClientes, (x: any) => {
            return x.clave == this.state.cliente;
        });
        return (this.props.isloading ? <Loading mensage={"Procesando por favor espere..."} /> :
            < div >
                <div className="card img-fondo-card" >
                    <div style={{ padding: 1 }} className="card-body">
                        <div style={{ margin: 0, padding: 2 }} className="row">
                            <div className="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div className="h6"> <span style={{ backgroundColor: "#0e7ec3", color: "#fff" }} className="label label-table label-secondary">{this.state.textoDetalle}</span> <span style={{ backgroundColor: "#555454", color: "#fff" }} className="label label-table label-secondary">{this.state.mercado ? this.state.mercado : "Todos"}</span></div>
                            </div>
                            <div className="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div className="h6"> <span style={{ backgroundColor: "#0e7ec3", color: "#fff" }} className="label label-table label-secondary">Cliente</span>   <span style={{ backgroundColor: "#555454", color: "#fff" }} className="label label-table label-secondary">{cliente ? cliente.clave + " - " + cliente.descripcion : "Todos"}</span> </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div className="card img-fondo-card" >
                    <div style={{ padding: 1 }} className="card-body">
                        <div className="row" style={{ display: "block", paddingRight: 15, marginBottom: 10 }}>
                            <Button
                                id={"btn-pz"}
                                style={{ textTransform: "unset", backgroundColor: "#555454", color: "#FFFFFF", marginRight: '5px', float: 'right' }}
                                variant="contained"
                                size="small"
                                startIcon={<i className="fas fa-tape fa-sm"></i>}
                                onClick={() => {
                                    $("#btn-lb").css({ backgroundColor: "#555454" });
                                    $("#btn-kg").css({ backgroundColor: "#555454" });
                                    $("#btn-pz").css({ backgroundColor: "#0e7ec3" });
                                    this.setState({ unidadPeso: "PZ" })
                                }}
                            > PZ
                                        </Button>
                            <Button
                                id={"btn-kg"}
                                style={{ textTransform: "unset", backgroundColor: "#0e7ec3", color: "#FFFFFF", marginRight: '5px', float: 'right' }}
                                variant="contained"
                                size="small"
                                startIcon={<i className="fas fa-weight fa-sm"></i>}
                                onClick={() => {
                                    $("#btn-kg").css({ backgroundColor: "#0e7ec3" });
                                    $("#btn-lb").css({ backgroundColor: "#555454" });
                                    $("#btn-pz").css({ backgroundColor: "#555454" });
                                    this.setState({ unidadPeso: "KG" })
                                }}
                            > KG
                                        </Button>
                            <Button
                                id={"btn-lb"}
                                style={{ textTransform: "unset", backgroundColor: "#555454", color: "#FFFFFF", marginRight: '5px', float: 'right' }}
                                variant="contained"
                                size="small"
                                startIcon={<i className="fas fa-balance-scale-right fa-sm"></i>}
                                onClick={() => {
                                    $("#btn-lb").css({ backgroundColor: "#0e7ec3" });
                                    $("#btn-kg").css({ backgroundColor: "#555454" });
                                    $("#btn-pz").css({ backgroundColor: "#555454" });
                                    this.setState({ unidadPeso: "LB" })
                                }}
                            > LB
                                        </Button>

                            <Button
                                style={{ textTransform: "unset", backgroundColor: "#555454", color: "#FFFFFF", float: 'right', marginRight: '5px' }}
                            variant="contained"
                            size="small"
                            startIcon={<i className="fas fa-file-excel fa-sm" />}
                            onClick={() => { this.props.requestDowloadExcel(this.state.mercado, this.state.cliente, this.state.vendedor) }}
                        > Excel
                        </Button>
                            <Button
                                style={{ textTransform: "unset", backgroundColor: "#555454", color: "#FFFFFF", float: 'right', marginRight: '5px' }}
                                variant="contained"
                                size="small"
                                startIcon={<i className="fas fa-reply fa-sm" />}
                                onClick={() => {
                                    window.history.go(-1);
                                    //this.props.history.push({
                                    //    pathname: `/SeguimientoOC`,
                                    //    state: {
                                    //        cliente: this.state.cliente
                                    //    }
                                    //});
                                }}
                            > Regresar
                            </Button>
                        </div>
                        <div className="row">
                        <Tabla
                            registros={this.props.registros}
                            columnas={columns}
                            filterable={true}
                        />                       
                        </div>
                    </div>
                </div>
        </div>);
    }
}
export default connect(
    (state: ApplicationState) => state.InventarioDetallesStore, InventarioDetallesStore.actionCreators
)(InventariosDetalle as any);