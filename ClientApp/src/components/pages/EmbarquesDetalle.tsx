﻿import * as React from 'react';
import numeral from 'numeral';
import * as _ from 'lodash';
import Tabla from '../common/Tabla';

import { Button} from '@material-ui/core';

import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../../store';
import * as EmbarqueDetallesStore from '../../store/EmbarqueDetallesStore';
import Loading from '../common/Loading';

type EmbarquesDetalleProps =
    EmbarqueDetallesStore.EmbarqueDetallesStoreState &
    typeof EmbarqueDetallesStore.actionCreators &
    RouteComponentProps<{}>;

interface EmbarquesDetalleState {
    grupo: string,
    cliente: string,
    vendedor: any,
    catalogoClientes: any,
    unidadPeso: string
}
class EmbarquesDetalle extends React.Component<EmbarquesDetalleProps, EmbarquesDetalleState>{
    constructor(props: any) {
        super(props);
        this.state = {
            grupo: "",
            cliente: "",
            vendedor: "",
            catalogoClientes: [],
            unidadPeso: "KG"
        };
    }
    componentDidMount() {
        var title: any = document.getElementById('title-page');
        title.innerHTML = "Historial de embarques por periodo";
        const state = (this.props.location as any).state;
        this.setState({ grupo: state.grupo, cliente: state.cliente, vendedor: state.vendedor, catalogoClientes: state.catalogoClientes });
        this.props.requestInformacionInicialEmbarqueDeta(state.grupo, state.cliente, state.vendedor);
    }

    public meses_txt = (texto: any) => {
        const months = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];
        let arr = texto.split('-');
        return months[Number(arr[1]) - 1] + "-" + arr[0];
    }

    componentWillReceiveProps(props: any) {

    }
    public render() {
        //mes actual
        const months = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];
        let date = new Date();
        let mm = date.getMonth();
        let yy = date.getFullYear();
        let fechas_array = [];
        for (let i = 0; i <= 6; i++) {
            let mmx = mm - i >= 0 ? (mm - i) : (12 + mm - i);
            let aax = mm - i >= 0 ? yy : (yy - 1);
            fechas_array[i] = months[mmx] + "-" + aax;
        }

        const columns = [

            {
                Header: 'Num Parte',
                accessor: 'ClienteNoParte',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Código',
                accessor: 'CodigoArticuloSAP',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Forma',
                accessor: 'Forma',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Tipo Mat',
                accessor: 'TipodeMaterial',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Grado',
                accessor: 'Grado',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" },
                minWidth: 150
            },
           
            {
                Header: 'Cal',
                accessor: 'Calibre',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Espesor',
                accessor: 'Espesor',
                Cell: (row: any) => {
                    return numeral(row.value).format('0,0.00');
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: 'Ancho',
                accessor: 'Ancho',
                Cell: (row: any) => {
                    return numeral(row.value).format('0,0.00');
                },
                headerStyle: { fontWeight: "bold" },
                //minWidth : 200
            },
            {
                Header: 'Largo',
                accessor: 'Largo',
                Cell: (row: any) => {
                    return numeral(row.value).format('0,0.00');
                },
                headerStyle: { fontWeight: "bold" },
                //minWidth: 200
            },
            {
                Header: fechas_array[6],
                accessor: '_Cantidad_6',
                Cell: (row: any) => {
                    var valor = this.state.unidadPeso == "KG" ? row.original._Cantidad_6 * 1000 : (this.state.unidadPeso == "LB" ? row.original._Cantidad_6 * 1000 * 2.20462 : row.original._Cantidad_6_pz);
                    return <div style={{ textAlign: "right" }}>{numeral(valor).format('0,0')}</div>;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: fechas_array[5],
                accessor: '_Cantidad_5',
                Cell: (row: any) => {
                    var valor = this.state.unidadPeso == "KG" ? row.original._Cantidad_5 * 1000 : (this.state.unidadPeso == "LB" ? row.original._Cantidad_5 * 1000 * 2.20462 : row.original._Cantidad_5_pz);
                    return <div style={{ textAlign: "right" }}>{numeral(valor).format('0,0')}</div>;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: fechas_array[4],
                accessor: '_Cantidad_4',
                Cell: (row: any) => {
                    var valor = this.state.unidadPeso == "KG" ? row.original._Cantidad_4 * 1000 : (this.state.unidadPeso == "LB" ? row.original._Cantidad_4 * 1000 * 2.20462 : row.original._Cantidad_4_pz);
                    return <div style={{ textAlign: "right" }}>{numeral(valor).format('0,0')}</div>;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: fechas_array[3],
                accessor: '_Cantidad_3',
                Cell: (row: any) => {
                    var valor = this.state.unidadPeso == "KG" ? row.original._Cantidad_3 * 1000 : (this.state.unidadPeso == "LB" ? row.original._Cantidad_3 * 1000 * 2.20462 : row.original._Cantidad_3_pz);
                    return <div style={{ textAlign: "right" }}>{numeral(valor).format('0,0')}</div>;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: fechas_array[2],
                accessor: '_Cantidad_2',
                Cell: (row: any) => {
                    var valor = this.state.unidadPeso == "KG" ? row.original._Cantidad_2 * 1000 : (this.state.unidadPeso == "LB" ? row.original._Cantidad_2 * 1000 * 2.20462 : row.original._Cantidad_2_pz);
                    return <div style={{ textAlign: "right" }}>{numeral(valor).format('0,0')}</div>;
                },
                headerStyle: { fontWeight: "bold" }
            },
            {
                Header: fechas_array[1],
                accessor: '_Cantidad_1',
                Cell: (row: any) => {
                    var valor = this.state.unidadPeso == "KG" ? row.original._Cantidad_1 * 1000 : (this.state.unidadPeso == "LB" ? row.original._Cantidad_1 * 1000 * 2.20462 : row.original._Cantidad_1_pz);
                    return <div style={{ textAlign: "right" }}>{numeral(valor).format('0,0')}</div>;
                },
                headerStyle: { fontWeight: "bold" },
            //    minWidth: 200
            },
            {
                Header: fechas_array[0],
                accessor: '_Cantidad_0',
                Cell: (row: any) => {
                    var valor = this.state.unidadPeso == "KG" ? row.original._Cantidad_0 * 1000 : (this.state.unidadPeso == "LB" ? row.original._Cantidad_0 * 1000 * 2.20462 : row.original._Cantidad_0_pz);
                    return <div style={{ textAlign: "right", paddingRight: '20px' }}>{numeral(valor).format('0,0')}</div>;
                },
                headerStyle: { fontWeight: "bold" },
            //    minWidth: 200
            }
              
        ];
        var cliente = _.find(this.state.catalogoClientes, (x: any) => {
            return x.clave == this.state.cliente;
        });

        if (!this.state.cliente) {
            columns.unshift({
                Header: 'Cliente',
                accessor: 'NombreCortoSN',
                Cell: (row: any) => {
                    return row.value;
                },
                headerStyle: { fontWeight: "bold" },
                minWidth: 250
            })
        }
        return (this.props.isloading ? <Loading mensage={"Procesando por favor espere..."} /> :
            < div >
                <div className="card img-fondo-card" >
                    <div style={{ padding: 1 }} className="card-body">
                        <div style={{ margin: 0, padding: 2 }} className="row">
                            <div className="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div className="h6"> <span style={{ backgroundColor: "#0e7ec3", color: "#fff" }} className="label label-table label-secondary">Periodo</span> <span style={{ backgroundColor: "#555454", color: "#fff" }} className="label label-table label-secondary">{this.state.grupo ? this.meses_txt(this.state.grupo) : "Todos"}</span></div>
                            </div>
                            <div className="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div className="h6"> <span style={{ backgroundColor: "#0e7ec3", color: "#fff" }} className="label label-table label-secondary">Cliente</span>   <span style={{ backgroundColor: "#555454", color: "#fff" }} className="label label-table label-secondary">{cliente ? cliente.clave + " - " + cliente.descripcion : "Todos"}</span> </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div className="card img-fondo-card" >
                    <div style={{ padding: 1 }} className="card-body">
                        <div className="row" style={{ display: "block", paddingRight: 15, marginBottom: 10 }}>
                            <Button
                                id={"btn-kg"}
                                style={{ textTransform: "unset", backgroundColor: "#0e7ec3", color: "#FFFFFF", marginRight: '5px', float: 'right' }}
                                variant="contained"
                                size="small"
                                startIcon={<i className="fas fa-weight fa-sm"></i>}
                                onClick={() => {
                                    $("#btn-kg").css({ backgroundColor: "#0e7ec3" });
                                    $("#btn-lb").css({ backgroundColor: "#555454" });
                                    $("#btn-pz").css({ backgroundColor: "#555454" });
                                    this.setState({ unidadPeso: "KG" })
                                }}
                            > KG
                                        </Button>
                            <Button
                                id={"btn-lb"}
                                style={{ textTransform: "unset", backgroundColor: "#555454", color: "#FFFFFF", marginRight: '5px', float: 'right' }}
                                variant="contained"
                                size="small"
                                startIcon={<i className="fas fa-balance-scale-right fa-sm"></i>}
                                onClick={() => {
                                    $("#btn-lb").css({ backgroundColor: "#0e7ec3" });
                                    $("#btn-kg").css({ backgroundColor: "#555454" });
                                    $("#btn-pz").css({ backgroundColor: "#555454" });
                                    this.setState({ unidadPeso: "LB" })
                                }}
                            > LB
                                        </Button>
                            <Button
                                id={"btn-pz"}
                                style={{ textTransform: "unset", backgroundColor: "#555454", color: "#FFFFFF", marginRight: '5px', float: 'right' }}
                                variant="contained"
                                size="small"
                                startIcon={<i className="fas fa-tape fa-sm"></i>}
                                onClick={() => {
                                    $("#btn-lb").css({ backgroundColor: "#555454" });
                                    $("#btn-kg").css({ backgroundColor: "#555454" });
                                    $("#btn-pz").css({ backgroundColor: "#0e7ec3" });
                                    this.setState({ unidadPeso: "PZ" })
                                }}
                            > PZ
                                        </Button>

                            <Button
                                style={{ textTransform: "unset", backgroundColor: "#555454", color: "#FFFFFF", float: 'right', marginRight: '5px' }}
                                variant="contained"
                                size="small"
                                startIcon={<i className="fas fa-file-excel fa-sm" />}
                                onClick={() => { this.props.requestDowloadExcel(this.state.grupo, this.state.cliente, this.state.vendedor) }}
                            > Excel
                            </Button>
                            <Button
                                style={{ textTransform: "unset", backgroundColor: "#555454", color: "#FFFFFF", float: 'right', marginRight: '5px' }}
                                variant="contained"
                                size="small"
                                startIcon={<i className="fas fa-reply fa-sm" />}
                                onClick={() => {
                                    window.history.go(-1);
                                    //this.props.history.push({
                                    //    pathname: `/SeguimientoOC`,
                                    //    state: {
                                    //        cliente: this.state.cliente
                                    //    }
                                    //});
                                }}
                            > Regresar
                            </Button>
                        </div>
                        <div className="row">
                            <Tabla
                                registros={this.props.registros}
                                columnas={columns}
                                filterable={true}
                            />
                        </div>
                    </div>
                    </div>
        </div>);
    }
}
export default connect(
    (state: ApplicationState) => state.EmbarqueDetallesStore, EmbarqueDetallesStore.actionCreators
)(EmbarquesDetalle as any);