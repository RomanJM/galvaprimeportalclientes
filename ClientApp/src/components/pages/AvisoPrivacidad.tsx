﻿import * as React from 'react';

export default class AvisoPrivacidad extends React.Component<{}, {}>{
    componentDidMount() {
        var title: any = document.getElementById('title-page');
        title.innerHTML = "Aviso de privacidad";
    }
    public render() {
        return <div className={"card "} >
            <div className="card img-fondo-card">
                <div className="card-body">
                    <div style={{ display: "flex", alignItems: "center" }} className="row">
                        <div style={{ display: "flex", alignItems :"center" }} className="col col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <h4 style={{ textAlign: "justify"}} className="section-main-title">¿Quiénes somos?</h4>
                            
                         </div>
                        <div className="col col-lg-9 col-md-9 col-sm-12 col-xs-12">
                            <p style={{ textAlign: "justify", fontSize:16 }}>
                                <strong>Galvaprime  </strong>con domicilio en Calle Privada La Puerta # 2990 Interior 1
                                Parque Industrial La Puerta, C.P.66350, en la entidad de Santa Catarina
                                Nuevo León, país México, y portal de internet <a href="http://www.galvaprime.com" target="blank">www.galvaprime.com</a>, es
                                el responsable del uso y protección de sus datos personales, y al
                                respecto le informamos lo siguiente:
                            </p>
                        </div>
                    </div>
                    <div style={{ display: "flex", alignItems: "center" }} className="row">
                        <div  className="col col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <h4 style={{ textAlign: "justify" }} className="section-main-title">¿Para qué fines utilizaremos sus datos personales?</h4>
                        </div>
                        <div  className="col col-lg-9 col-md-9 col-sm-12 col-xs-12">
                            <p style={{ textAlign: "justify", marginBottom: 10, fontSize: 17 }}>
                                De manera adicional, utilizaremos su dirección de correo para las
                                siguientes finalidades secundarias que no son necesarias para el
                                servicio solicitado, pero que nos permiten y facilitan brindarle una mejor
                                atención:
                            </p>
                            <p style={{ textAlign: "justify", marginBottom: 10, fontSize: 17}}>
                                <ul className="list-group">
                                    <li ><i className="fas fa-check"></i> Inscripción en boletín de ofertas de la empresa</li>
                                    <li><i className="fas fa-check"></i> Verificación de identidad del usuario</li>
                                    <li><i className="fas fa-check"></i> Mercadotecnia o publicitaria</li>
                                    <li><i className="fas fa-check"></i> Prospección comercial</li>
                                </ul>
                            </p>
                            <p style={{ textAlign: "justify", marginBottom: 10, fontSize: 17}}>
                                En caso de que no desee que sus datos personales sean tratados para
                                estos fines secundarios, desde este momento usted nos puede comunicar
                                lo anterior a través del siguiente mecanismo:
                            </p>
                            <p style={{ textAlign: "justify", marginBottom: 10, fontSize: 17 }}>
                                Vía correo electrónico a:  <strong> ventas@galvaprime.com  </strong>
                            </p>
                            <p style={{ textAlign: "justify", fontSize: 17 }}>
                                La negativa para el uso de sus datos personales para estas finalidades no
                                podrá ser un motivo para que le neguemos los servicios y productos que
                                solicita o contrata con nosotros.
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>;
    }
}