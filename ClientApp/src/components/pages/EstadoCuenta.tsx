﻿
import * as React from 'react';
import numeral from 'numeral';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../../store';
import * as EstadoCuentaStore from '../../store/EstadoCuentaStore';
import { Chart } from "react-google-charts";
import Select from 'react-select';
import GraficaGral from '../common/GraficaGral';
/**
 * Components
 * */
import { CardBody, CardHeader, Row, Col, CardTitle } from 'reactstrap';
import Loading from '../common/Loading';
import { Card, Collapse, IconButton, Button, InputLabel, MenuItem, FormControl, Zoom, Chip, CardActionArea, CardContent, Typography } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import { ListItem, ListItemIcon, List, ListItemSecondaryAction, ListItemText } from '@material-ui/core';

type EstadoCuentaProps =
    EstadoCuentaStore.EstadoCuentaStoreState &
    typeof EstadoCuentaStore.actionCreators &
    RouteComponentProps<{}>;
interface EstadoCuentaState {
    cliente: any,
    vendedor: any,
    nombreCliente: string,
    open: boolean,
    ValorTxt: any
}

class EstadoCuenta extends React.Component<EstadoCuentaProps, EstadoCuentaState>{

    constructor(props: any) {
        super(props);
        this.state = {
            cliente: "",
            vendedor: "",
            nombreCliente: "",
            open: true,
            ValorTxt: "USD"
        };
        this.InformacionIncial();
    }
    public filtrar = () => {
        var cliente = _.find(this.props.clientes, (x: any) => {
            return x.clave == this.state.cliente;
        });
        this.setState({ nombreCliente: cliente ? cliente.descripcion : "", open: true });
        this.props.requestInformacionInicial(this.state.cliente ? this.state.cliente.value : "", this.state.vendedor ? this.state.vendedor.value : "");
    }
    componentDidMount() {
       
    }
    public InformacionIncial = () => {
        //console.log("se ejecuta");
        var title: any = document.getElementById('title-page');
        title.innerHTML = "Estado de cuenta";
        var rol = window.localStorage.getItem("rolusuario");
        var cliente: any = "";
        if (rol == "CLI") {
            cliente = window.localStorage.getItem("Usuario_id");
        }
        console.log("cliente::" + cliente);
        var vendedor: any = "";
        if (rol == "VEN") {
            vendedor = window.localStorage.getItem("ZEmpleado");
        }
        this.setState({ cliente: cliente ? { value: cliente, label: '' } : '', vendedor: vendedor ? { value: vendedor, label: '' } : '' });
        this.props.requestInformacionInicial(cliente, vendedor);
        this.props.requestClientes(vendedor);
        this.props.requestVendedores();

    }


    public render() {
        var primerRegistro = _.find(this.props.registros, (item: any) => {
            return item;
        });
        var valor = primerRegistro ? primerRegistro.Valor2 : 0;
        var colores: any = [];
        var clientes = [];
        if (this.props.clientes.length > 0)
            clientes.push({ value: "0", label: '- TODO -' });
        this.props.clientes.map((item: any) => {
            clientes.push({ value: item.clave, label: item.descripcion })
        });
        var vendedores = [];
        if (this.props.vendedores.length > 0)
            vendedores.push({ value: 0, label: '- TODO -' });
        this.props.vendedores.map((item: any) => {
            vendedores.push({ value: item.clave, label: item.descripcion })
        });
        var valorTxt = valor == 1 ? "MXN" : "USD";
        var registrosFilter = this.props.registros ? _.filter(this.props.registros, (item: any) => {
            return item.ValorTxt.substr(item.ValorTxt.length - 3) === (valor == 0 ? this.state.ValorTxt : valorTxt);
        }) : [];
       
        var total = 0;

        var dataset = [];
        dataset.push(['', '']);
        var data = registrosFilter.map((item: any) => {
            total = total + item.Valor;
            if (item.Grupo == "Por Vencer")
                colores.push("#00B050")
            else if (item.Grupo == "De 1 a 15")
                colores.push("#FFFF00")
            else if (item.Grupo == "De 15 a 30")
                colores.push("#FFC000")
            else if (item.Grupo == "De 30 a 60")
                colores.push("#FF6600")
            else if (item.Grupo == "De 60 a 90")
                colores.push("#A50021")
            else
                colores.push("#FF0000")
            dataset.push([item.Grupo, item.Valor]);
            return item.Valor;
        });
        const labels = registrosFilter.map((item: any) => {
            return item.Grupo;
        });
        const props = this.props;
        var rol = window.localStorage.getItem("rolusuario");
        var styilos = {
            option: (styles: any, { data, isDisabled, isFocused, isSelected }: { data: any, isDisabled: any, isFocused: any, isSelected: any }) => {
                return {
                    ...styles,
                    backgroundColor: isDisabled
                        ? null
                        : isSelected
                            ? '#0e7ec3'
                            : isFocused
                                ? '#B2D4FF'
                                : null,
                    ':active': {
                        ...styles[':active'],
                        backgroundColor: '#0e7ec3',
                    },
                };
            },
        };
        var state = this.state;
        var height = window.screen.height - 400;
        return (
            this.props.isloading ? <Loading mensage={"Procesando por favor espere..."} /> :
                <div>

                    <Zoom in={true} timeout={1500}>
                        <div className="card img-fondo-card" style={{ padding: '5px', margin: '0px 0px 5px 0px' }}>
                            <div className="card-body">
                                <div className="row" >
                                    {(rol == "ADM" || rol == "JF1") ?
                                        <div className="col col-sm-12 col-xs-12 col-md-4 col-lg-4" style={{ margin: '1px 0' }} >
                                            <Select
                                                className="react-select"
                                                value={this.state.vendedor}
                                                options={vendedores}
                                                onChange={(valor: any) => {
                                                    this.setState({ vendedor: valor, cliente: null });
                                                    this.props.requestFiltraClientes(valor.value);
                                                    this.props.requestInformacionInicial( "", valor ? valor.value : '');
                                                }}
                                                placeholder={"Seleccionar vendedor"}
                                                defaultValue={{ value: 0, label: '- TODO -' }}
                                                styles={styilos}
                                            />

                                        </div>
                                        : null}

                                    {(rol == "ADM" || rol == "VEN" || rol == "JF1") ?
                                        <div className="col col-sm-12 col-xs-12 col-md-4 col-lg-4" style={{ margin: '1px 0' }} >
                                            <Select
                                                value={this.state.cliente}
                                                options={clientes}
                                                onChange={(valor: any) => {
                                                    this.setState({ cliente: valor });
                                                    this.props.requestInformacionInicial(valor ? valor.value : '', this.state.vendedor ? this.state.vendedor.value : '');
                                                }}
                                                placeholder={"Seleccionar cliente"}
                                                defaultValue={{ value: "0", label: '- TODO -' }}
                                                styles={styilos}
                                            />

                                        </div>
                                        : null}

                                    <div className="col col-sm-12 col-xs-12 col-md-4 col-lg-4" style={{ margin: '1px 0' }} >
                                        {valor == 1 || valor == 0? <Button
                                            id={"btn-kg"}
                                            style={{ textTransform: "unset", backgroundColor: valor == 1 ?  "#0e7ec3" : "#555454" , color: "#FFFFFF", float: 'right', marginRight: '5px' }}
                                            variant="contained"
                                            size="small"
                                            startIcon={<i className="fas fa-dollar-sign fa-sm"></i>}
                                            onClick={() => {
                                                $("#btn-kg").css({ backgroundColor: "#0e7ec3" });
                                                $("#btn-lb").css({ backgroundColor: "#555454" });
                                                this.setState({ ValorTxt: "MXN" });
                                            }}
                                        > MXN
                                        </Button> :  null}
                                        { valor == 2 || valor == 0 ? <Button
                                            id={"btn-lb"}
                                            style={{ textTransform: "unset", backgroundColor: valor == 2 || valor == 0 ? "#0e7ec3" : "#555454", color: "#FFFFFF", float: 'right', marginRight: '5px' }}
                                            variant="contained"
                                            size="small"
                                            startIcon={<i className="fas fa-dollar-sign fa-sm"></i>}
                                            onClick={() => {
                                                $("#btn-lb").css({ backgroundColor: "#0e7ec3" });
                                                $("#btn-kg").css({ backgroundColor: "#555454" });
                                                this.setState({ ValorTxt: "USD" });
                                            }}
                                        > USD
                                        </Button> :  null}
                                        <Button
                                            style={{ textTransform: "unset", backgroundColor: "#555454", color: "#FFFFFF", float: 'right', marginRight: '5px' }}
                                            variant="contained"
                                            size="small"
                                            startIcon={<i className="fas fa-file-excel fa-sm" />}
                                            onClick={() => { this.props.requestDowloadExcel(this.state.cliente ? this.state.cliente.value : '', this.state.vendedor ? this.state.vendedor.value : 0) }}
                                        > Excel
                                </Button>
                                        <Button
                                            style={{ textTransform: "unset", backgroundColor: "#0e7ec3", color: "#FFFFFF", float: 'right', marginRight: '5px' }}
                                            variant="contained"
                                            size="small"
                                            startIcon={<i className="fas fa-search"></i>}
                                            onClick={() => { this.filtrar() }}
                                        > Buscar
                                            </Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Zoom>

                    <Zoom in={true} timeout={1500}>
                        <div className="card img-fondo-card" >
                            <div className="card-body">
                                <div style={{ display: "block", margin: 10 }} className="row">
                                    <div className="col col-sm-12 col-xs-12 col-md-9 col-lg-9">
                                        <GraficaGral
                                            label={'Estado de cuenta'}
                                            labels={labels}
                                            data={data}
                                            backgroundColor={colores}
                                            onclick={(items: any) => {
                                                if (items[0]) {
                                                    var index = items[0]._index;
                                                    const merca = props.registros[index];
                                                    props.history.push({
                                                        pathname: `/EstadoCuentaDetalle`,
                                                        state: {
                                                            periodo: merca ? merca.ValorTxt : "Debit0USD",
                                                            vendedor: state.vendedor ? state.vendedor.value : 0,
                                                            cliente: state.cliente ? state.cliente.value : '',
                                                            catalogoClientes: props.clientes
                                                        }
                                                    });
                                                }
                                            }}
                                        />
                                        {/*<Chart*/}
                                        {/*    width={"100%"}*/}
                                        {/*    height={height}*/}
                                        {/*    chartType="PieChart"*/}
                                        {/*    loader={<div>Loading Chart</div>}*/}
                                        {/*    data={dataset}*/}
                                        {/*    options={{*/}
                                        {/*       /* legend: { position: 'top', alignment: 'start' }
                                        {/*        title: '',*/}
                                        {/*        colors: colores,*/}
                                        {/*        chartArea: {*/}
                                        {/*            left: "0",*/}
                                        {/*            top: "0",*/}
                                        {/*            height: "100%",*/}
                                        {/*            width: "100%"*/}
                                        {/*        }*/}
                                                
                                        {/*    }}*/}
                                        {/*    rootProps={{ 'data-testid': '2' }}*/}
                                        {/*/>*/}
                                        {/*<Pie*/}
                                        {/*    height={window.screen.height - 600}*/}
                                        {/*    data={{*/}
                                        {/*        labels: labels,*/}
                                        {/*        datasets: [{*/}
                                        {/*            label: '',*/}
                                        {/*            data: data,*/}
                                        {/*            backgroundColor: colores*/}

                                        {/*        }]*/}
                                        {/*    }}*/}
                                        {/*    options={*/}
                                        {/*        {*/}
                                        {/*            onClick: (e, items: any) => {*/}
                                        {/*                if (items[0]) {*/}
                                        {/*                    var index = items[0]._index;*/}
                                        {/*                    const merca = props.registros[index];*/}
                                        {/*                    props.history.push({*/}
                                        {/*                        pathname: `/EstadoCuentaDetalle`,*/}
                                        {/*                        state: {*/}
                                        {/*                            periodo: merca ? merca.ValorTxt : "Debit0USD",*/}
                                        {/*                            vendedor: state.vendedor ? state.vendedor.value : 0,*/}
                                        {/*                            cliente: state.cliente ? state.cliente.value : '',*/}
                                        {/*                            catalogoClientes: props.clientes*/}
                                        {/*                        }*/}
                                        {/*                    });*/}
                                        {/*                }*/}
                                        {/*            },*/}
                                        {/*            tooltips: {*/}
                                        {/*                callbacks: {*/}
                                        {/*                    label: function (tooltipItem: any, data: any) {*/}
                                        {/*                        var dataLabel = data.labels[tooltipItem.index];*/}
                                        {/*                        var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];*/}
                                        {/*                        return dataLabel + ': $' + numeral(value).format('0,0');*/}
                                        {/*                    }*/}
                                        {/*                },*/}
                                        {/*                backgroundColor: '#555454',*/}
                                        {/*                titleFontFamily: 'Lato',*/}
                                        {/*                bodyFontFamily: 'Lato',*/}
                                        {/*                footerFontFamily: 'Lato'*/}

                                        {/*            },*/}
                                        {/*            animation: {*/}
                                        {/*                duration: 1000,*/}
                                        {/*                easing: 'linear'*/}
                                        {/*            }*/}
                                        {/*        }*/}
                                        {/*    }*/}
                                        {/*/>*/}
                                    </div>
                                    <div className="col col-sm-12 col-xs-12 col-md-3 col-lg-3">
                                        <div className="row">
                                            <div className="col-xs-12 col-md-12 col-sm-12 col-lg-12">
                                                <Card style={{ textAlign: "center" }} onClick={() => {
                                                    var rol = window.localStorage.getItem("rolusuario");
                                                    console.log("ClicTOTAL. rol:" + rol);
                                                    var cliente: any = "";
                                                    if (rol == "CLI") {
                                                        cliente = window.localStorage.getItem("Usuario_id");
                                                    } else
                                                        cliente = this.state.cliente ? this.state.cliente.value : '';
                                                    console.log("ClicTOTAL. cliente:" + cliente);
                                                    var estado = this.state;
                                                    props.history.push({
                                                        pathname: `/EstadoCuentaDetalle`,
                                                        state: {
                                                            vendedor: this.state.vendedor ? this.state.vendedor.value : 0,
                                                            //cliente: this.state.cliente ? this.state.cliente.value : '',
                                                            cliente: cliente,
                                                            periodo: "",
                                                            catalogoClientes: props.clientes
                                                        }
                                                    });
                                                }} >
                                                    <CardActionArea>
                                                        <CardContent style={{ padding: 0 }}>
                                                            <Typography gutterBottom variant="h5" component="h2">
                                                                <Chip
                                                                    label={numeral(total).format("$0,0")}
                                                                    color="primary"
                                                                    variant="outlined"
                                                                    style={{ fontSize: '16px', fontWeight: 'bold' }}
                                                                />
                                                            </Typography>
                                                            <Typography gutterBottom variant="h6" component="h6">
                                                                Total
                                                        </Typography>
                                                        </CardContent>
                                                    </CardActionArea>
                                                </Card>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-xs-12 col-md-12 col-sm-12 col-lg-12">
                                                <Card style={{ textAlign: "center", marginTop: 5 }}  >
                                                    <CardActionArea>
                                                        <CardContent>
                                                            <List
                                                                component="nav"
                                                            >
                                                                {registrosFilter.map((x: any) => {
                                                                    return <ListItem style={{ padding: 0, margin: '0px 0px 8px 0px' }}>
                                                                        <ListItemText style={{ padding: 0, margin: 0 }}
                                                                            primary={x.Grupo}
                                                                            secondary={numeral(x.Valor).format("$0,0")}
                                                                        > </ListItemText>
                                                                        <ListItemSecondaryAction>
                                                                            <IconButton edge="end" aria-label="ir">
                                                                                <i onClick={() => {
                                                                                    props.history.push({
                                                                                        pathname: `/EstadoCuentaDetalle`,
                                                                                        state: {
                                                                                            vendedor: this.state.vendedor ? this.state.vendedor.value : 0,
                                                                                            cliente: this.state.cliente ? this.state.cliente.value : '',
                                                                                            catalogoClientes: this.props.clientes,
                                                                                            periodo: x.ValorTxt
                                                                                        }
                                                                                    });
                                                                                }} className="fas fa-location-arrow"></i>
                                                                            </IconButton>
                                                                        </ListItemSecondaryAction>
                                                                    </ListItem>
                                                                })}

                                                            </List>
                                                        </CardContent>
                                                    </CardActionArea>
                                                </Card>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Zoom>
                </div>
        );
    }
}

export default connect(
    (state: ApplicationState) => state.EstadoCuentaStore, EstadoCuentaStore.actionCreators
)(EstadoCuenta as any);