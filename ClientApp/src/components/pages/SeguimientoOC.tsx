﻿import * as React from 'react';
import numeral from 'numeral';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../../store';
import * as SeguimientoOCStore from '../../store/SeguimientoOCStore';
import { Bar } from 'react-chartjs-2';
import Select from 'react-select';
import GraficaGral from '../common/GraficaGral';
/**
 * Components
 * */
import { CardBody, CardHeader, Row, Col, CardTitle } from 'reactstrap';
import Loading from '../common/Loading';
import { Card,Collapse ,IconButton,Button,InputLabel, MenuItem, FormControl, Zoom, Chip, CardActionArea, CardContent, Typography } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import { ListItem, ListItemIcon, List, ListItemSecondaryAction, ListItemText} from '@material-ui/core';

type SeguimientoOCProps =
    SeguimientoOCStore.SeguimientoOCStoreState &
    typeof SeguimientoOCStore.actionCreators &
    RouteComponentProps<{}>;
interface SeguimientoOCState {
    cliente: any,
    vendedor: any,
    nombreCliente: string,
    open: boolean,
    unidadPeso : string
}

class SeguimientoOC extends React.Component<SeguimientoOCProps, SeguimientoOCState>{

    constructor(props: any) {
        //if (props.history.location.state) {
        //    console.log(props.history.location.state);
        //}
        super(props);
        this.state = {
            cliente: null,
            vendedor: null,
            nombreCliente: "",
            open: true,
            unidadPeso : "KG"
        };
    }
    public filtrar = () => {
        var cliente = _.find(this.props.clientes, (x: any) => {
            return x.clave == this.state.cliente ? this.state.cliente.value : "";
        });
        this.setState({nombreCliente : cliente ? cliente.descripcion : "", open: true});
        this.props.requestInformacionInicial(this.state.cliente ? this.state.cliente.value:'', this.state.vendedor ? this.state.vendedor.value:'');
    }
    componentDidMount() {
        var rol = window.localStorage.getItem("rolusuario");
        var cliente: any = "";
        if (rol == "CLI") {
            cliente = window.localStorage.getItem("Usuario_id");
        }
        var vendedor: any = "";
        if (rol == "VEN") {
            vendedor = window.localStorage.getItem("ZEmpleado");
        }

        var title: any = document.getElementById('title-page');
        title.innerHTML = "Seguimiento a órdenes de compras abiertas";

        this.setState({ cliente: cliente ? { value: cliente, label: '' } : '', vendedor: vendedor ?{ value: vendedor, label: '' }  : ''});
        this.props.requestInformacionInicial(cliente, vendedor);
        this.props.requestClientes(vendedor);
        this.props.requestVendedores();
    }
    public render() {
        var unidadPeso = this.state.unidadPeso;
        var clientes = [];
        if (this.props.clientes.length > 0)
            clientes.push({ value: "0", label: '- TODO -' });
        this.props.clientes.map((item: any) => {
            clientes.push({ value: item.clave, label: item.descripcion })
        });
        var vendedores = [];
        if (this.props.vendedores.length > 0)
            vendedores.push({ value: 0, label: '- TODO -' });
        this.props.vendedores.map((item: any) => {
            vendedores.push({ value: item.clave, label: item.descripcion })
        });
        var total = 0;
        const data = this.props.registros.map((item: any) => {          
            return this.state.unidadPeso == "KG" ? item.Valor : this.state.unidadPeso == "LB" ? item.Valor2 : item.Valor3;
        });
        this.props.registros.map((item: any) => {
            total = total + (this.state.unidadPeso == "KG" ? item.Valor : this.state.unidadPeso == "LB" ? item.Valor2 : item.Valor3);
        });
        const labels = this.props.registros.map((item: any) => {
            return item.Grupo;
        });
        var rol = window.localStorage.getItem("rolusuario");
        const props = this.props;
        var cliente = this.state.cliente;
        var styilos = {
            option: (styles: any, { data, isDisabled, isFocused, isSelected }: { data: any, isDisabled: any, isFocused: any, isSelected: any }) => {
                return {
                    ...styles,
                    backgroundColor: isDisabled
                        ? null
                        : isSelected
                            ? '#0e7ec3'
                            : isFocused
                                ? '#B2D4FF'
                                : null,
                    ':active': {
                        ...styles[':active'],
                        backgroundColor:'#0e7ec3',
                    },
                };
            },
        };
        return (
            this.props.isloading ? <Loading mensage={"Procesando por favor espere..."} /> :              
                <div>

                    <Zoom in={true} timeout={1500}>
                        <div className="card img-fondo-card" style={{ padding: '5px', margin: '0px 0px 5px 0px' }}>
                                <div className="card-body">
                                    <div className="row" >
                                    {(rol == "ADM" || rol == "JF1") ?
                                            <div className="col col-sm-12 col-xs-12 col-md-4 col-lg-4" style={{ margin:'1px 0' }} >
                                                <Select
                                                    className="react-select"
                                                    value={this.state.vendedor}
                                                    options={vendedores}
                                                    onChange={(valor: any) => {
                                                        this.setState({ vendedor: valor, cliente: null });
                                                        this.props.requestFiltraClientes(valor.value);
                                                        this.props.requestInformacionInicial("", valor ? valor.value : '');
                                                    }}
                                                    placeholder={"Seleccionar vendedor"}
                                                    defaultValue={{ value: 0, label: '- TODO -' }}
                                                    styles={styilos}
                                                />

                                            </div>
                                         : null}

                                    {(rol == "ADM" || rol == "VEN" || rol == "JF1") ?
                                            <div className="col col-sm-12 col-xs-12 col-md-4 col-lg-4" style={{ margin: '1px 0' }} >
                                                <Select
                                                    value={this.state.cliente}
                                                    options={clientes}
                                                    onChange={(valor: any) => {
                                                        this.setState({ cliente: valor });
                                                        this.props.requestInformacionInicial(valor ? valor.value : '',this.state.vendedor ? this.state.vendedor.value : '');
                                                    }}
                                                    placeholder={"Seleccionar cliente"}
                                                    defaultValue={{ value: "0", label: '- TODO -' }}
                                                    styles={styilos}
                                                />

                                            </div>
                                         : null}
                                        
                                        <div className="col col-sm-12 col-xs-12 col-md-4 col-lg-4" style={{ margin: '1px 0' }} >                                                   
                                            <Button
                                                id={"btn-kg"}
                                                style={{ textTransform: "unset", backgroundColor: "#0e7ec3", color: "#FFFFFF", marginRight: '5px', float: 'right' }}
                                                variant="contained"
                                                size="small"
                                                startIcon={<i className="fas fa-weight fa-sm"></i>}
                                                onClick={() => {
                                                    $("#btn-kg").css({ backgroundColor: "#0e7ec3" });
                                                    $("#btn-lb").css({ backgroundColor: "#555454" });
                                                    $("#btn-pz").css({ backgroundColor: "#555454" });
                                                    this.setState({ unidadPeso: "KG" })
                                                }}
                                            > KG
                                        </Button>
                                            <Button
                                                id={"btn-lb"}
                                                style={{ textTransform: "unset", backgroundColor: "#555454", color: "#FFFFFF", marginRight: '5px', float: 'right' }}
                                                variant="contained"
                                                size="small"
                                                startIcon={<i className="fas fa-balance-scale-right fa-sm"></i>}
                                                onClick={() => {
                                                    $("#btn-lb").css({ backgroundColor: "#0e7ec3" });
                                                    $("#btn-kg").css({ backgroundColor: "#555454" });
                                                    $("#btn-pz").css({ backgroundColor: "#555454" });
                                                    this.setState({ unidadPeso: "LB" })
                                                }}
                                            > LB
                                        </Button>
                                            <Button
                                                style={{ textTransform: "unset", backgroundColor: "#555454", color: "#FFFFFF", marginRight: '5px', float: 'right'}}
                                                variant="contained"
                                                size="small"
                                                startIcon={<i className="fas fa-file-excel fa-sm" />}
                                            onClick={() => { this.props.requestDowloadExcel(this.state.cliente.value, this.state.vendedor.value) }}
                                            > Excel
                                        </Button>
                                            <Button
                                                style={{ textTransform: "unset", backgroundColor: "#0e7ec3", color: "#FFFFFF", marginRight: '5px', float: 'right' }}
                                                variant="contained"
                                                size="small"
                                                startIcon={<i className="fas fa-search"></i>}
                                                onClick={() => { this.filtrar() }}
                                            > Buscar
                                            </Button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Zoom>

                    
                        <Zoom in={true} timeout={1500}>
                            <div className="card img-fondo-card" >
                                <div className="card-body">
                                <div style={{ display: "block", marginTop: 10 }} className="row">
                                    <div className="col col-sm-12 col-xs-12 col-md-9 col-lg-9">
                                        <GraficaGral
                                            label={'Órdenes de compras abiertas'}
                                            labels={labels}
                                            data={data}
                                            backgroundColor={[
                                                "#F4D03F",
                                                "#F2230B",
                                                "#52BE80",
                                                "#5DADE2"
                                            ]}
                                            onclick={(items: any) => {
                                                if (items[0]) {
                                                    var index = items[0]._index;
                                                    const vencimient = props.registros[index];
                                                    var cliente_e = cliente;
                                                    props.history.push({
                                                        pathname: `/SeguimientoOCDetalle`,
                                                        state: {
                                                            vencimiento: vencimient ? vencimient.Grupo : "Adelantos",
                                                            vendedor: this.state.vendedor ? this.state.vendedor.value: '',
                                                            cliente: cliente_e ? cliente_e.value : "",
                                                            catalogoClientes: props.clientes,
                                                            unidadPeso: unidadPeso
                                                        }
                                                    });
                                                }
                                            }}
                                        />
                                        </div>
                                        <div className="col col-sm-12 col-xs-12 col-md-3 col-lg-3">
                                            <div className="row">
                                                <div className="col-xs-12 col-md-12">
                                                    <Card style={{ textAlign: "center" }} onClick={() => {
                                                        var estado = this.state;
                                                        props.history.push({
                                                            pathname: `/SeguimientoOCDetalle`,
                                                            state: {
                                                                vendedor: estado.vendedor ? estado.vendedor : "",
                                                                cliente: estado.cliente ? estado.cliente.value : "",
                                                                unidadPeso: estado.unidadPeso,
                                                                catalogoClientes: props.clientes
                                                            }
                                                        });
                                                    }}>
                                                    <CardActionArea>
                                                        <CardContent style={{ padding: 0 }}>
                                                                <Typography gutterBottom variant="h5" component="h2">
                                                                    <Chip
                                                                    label={numeral(total).format("0,0")}
                                                                    color="primary"
                                                                    variant="outlined"
                                                                    style={{ fontSize: '16px', fontWeight:'bold' }}
                                                                    />
                                                                </Typography>
                                                                <Typography gutterBottom variant="h6" component="h6">
                                                                    Total {this.state.unidadPeso}
                                                                </Typography>
                                                            </CardContent>
                                                        </CardActionArea>

                                                    </Card>
                                                </div>
                                            </div>
                                            <div className="row">
                                            <div className="col-xs-12 col-md-12">
                                                <Card style={{ textAlign: "center", marginTop: 5 }} >
                                                    <CardActionArea>
                                                        <CardContent style={{ padding: 0 }}>
                                                            <List
                                                                component="nav"
                                                            >
                                                                {this.props.registros.map((x: any) => {
                                                                    return <ListItem  >
                                                                        <ListItemText
                                                                            style={{ fontSize: '16px', fontWeight: 'bold' }}
                                                                            primary={x.Grupo}
                                                                            secondary={this.state.unidadPeso == "KG" ? numeral(x.Valor).format("0,0") : this.state.unidadPeso == "LB" ? numeral(x.Valor2).format("0,0") : numeral(x.Valor3).format("0,0")}
                                                                        > </ListItemText>
                                                                        <ListItemSecondaryAction>
                                                                            <IconButton edge="end" aria-label="ir">
                                                                                <i onClick={() => {
                                                                                    props.history.push({
                                                                                        pathname: `/SeguimientoOCDetalle`,
                                                                                        state: {
                                                                                            vendedor: this.state.vendedor ? this.state.vendedor.value : '',
                                                                                            cliente: this.state.cliente ? this.state.cliente.value : '',
                                                                                            vencimiento: x.Grupo,
                                                                                            unidadPeso: this.state.unidadPeso,
                                                                                            catalogoClientes: this.props.clientes
                                                                                        }
                                                                                    });
                                                                                }} className="fas fa-location-arrow"></i>
                                                                            </IconButton>
                                                                        </ListItemSecondaryAction>
                                                                    </ListItem>
                                                                })}

                                                            </List>
                                                        </CardContent>
                                                    </CardActionArea>

                                                </Card>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Zoom> 
                </div>
            );
    }
}

export default connect(
    (state: ApplicationState) => state.SeguimientoOCStore, SeguimientoOCStore.actionCreators
)(SeguimientoOC as any);