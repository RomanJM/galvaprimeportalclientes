﻿import * as React from 'react';
import Select from 'react-select';


interface SelectFormProps {
    value: any;
    options: any[];
    onChange: any;
    styles?: any;
    placeholder?: string;
    defaultvalue?: any;
    isMulti?: boolean;
}
export default class SelectForm extends React.Component<SelectFormProps, any>{
    public render() {
        const estilos = {
            option: (styles: any, { isDisabled, isFocused, isSelected }: { data: any, isDisabled: any, isFocused: any, isSelected: any }) => {
                return {
                    ...styles,
                    backgroundColor: isDisabled
                        ? null
                        : isSelected
                            ? '#0e7ec3'
                            : isFocused
                                ? '#B2D4FF'
                                : null,
                    ':active': {
                        ...styles[':active'],
                        backgroundColor: '#0e7ec3',
                    },
                };
            },
        };
        return <div >
                    <Select
                        className="react-select"
                        value={this.props.value}
                        options={this.props.options}
                        onChange={(item: any) => {
                            this.props.onChange(item);
                        }}
                        placeholder={this.props.placeholder}
                        defaultValue={this.props.defaultvalue}
                        styles={estilos}
                        isMulti={this.props.isMulti}
                    />
              </div>;
    }
}