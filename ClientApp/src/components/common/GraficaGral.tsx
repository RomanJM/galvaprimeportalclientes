﻿import * as React from 'react';
import numeral from 'numeral';
import { Bar } from 'react-chartjs-2';

interface GraficaGralProps {
    label: any,
    labels: any[],
    data: any[],
    backgroundColor: any,
    onclick: any
}
export default class GraficaGral extends React.Component<GraficaGralProps, any>{
    public render() {
        return (
            <Bar
                height={window.screen.height - 400}
                data={{
                    labels: this.props.labels,
                    datasets: [{
                        label: this.props.label,
                        data: this.props.data,
                        /*backgroundColor: '#0e7ec3',*/
                        backgroundColor: this.props.backgroundColor,
                        pointStyle: 'circle',
                        hoverRadius: 5,
                        barThickness: 80,
                        radius: [20, 20, 20, 20, 20]

                    }]
                }}
                options={
                    {
                        rotation: 40,
                        maintainAspectRatio: false,
                        responsive: true,
                        scales: {
                            yAxes: [{
                                /*gridLines: { color: '#555454', offsetGridLines: true },*/
                                ticks: {
                                    beginAtZero: true,
                                    callback: function (value) {
                                        return numeral(value).format('0,0')
                                    },
                                    fontFamily: 'Lato',
                                    fontSize: 14
                                }

                            }],
                            xAxes: [{
                                gridLines: { display: false },
                                ticks: {
                                    fontFamily: 'Lato',
                                    fontSize: 14
                                }
                            }]
                        },
                        legend: {
                            display: false,
                            labels: {
                                fontFamily: 'Lato',
                                fontSize: 14
                            }
                        },
                        onClick: (e, items: any) => {
                            this.props.onclick(items);
                        },
                        tooltips: {
                            callbacks: {
                                label: function (tooltipItem: any, data: any) {
                                    let label = data.datasets[tooltipItem.datasetIndex].label || '';
                                    if (label) {
                                        label += ':* ';
                                    }
                                    label += numeral(tooltipItem.yLabel).format('0,0');
                                    return label;
                                }
                            },
                            backgroundColor: '#555454',
                            titleFontFamily: 'Lato',
                            bodyFontFamily: 'Lato',
                            footerFontFamily: 'Lato'

                        },
                        animation: {
                            duration: 1000,
                            easing: 'linear'
                        }
                    }
                }
                redraw={true}
            />
            );
    }
}