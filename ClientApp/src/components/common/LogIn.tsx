﻿import * as React from 'react';
import * as _ from 'lodash';
import * as url from '../../common';
import { RouteComponentProps } from 'react-router';
import { TextField, Zoom, Grid } from '@material-ui/core';
import axios from 'axios';
import * as toastr from './Toast';
import Loading from '../common/Loading';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

interface LogInState {
	user: string,
	password: string,
	loading: boolean,
	uservalidate: boolean,
	passwordvalidate: boolean,
	textouser: string,
	textopassword: string
}

export default class LogIn extends React.Component<RouteComponentProps<{}>, LogInState>{
	constructor(props: any) {
		super(props);
		this.state = {
			user: "",
			password: "",
			loading: false,
			uservalidate: false,
			passwordvalidate: false,
			textouser: "",
			textopassword: ""

		};
    }
	public LogIn = () => {
		
		if (!this.state.user) {
			this.setState({ uservalidate: true, textouser: "Usuario incorrecto" });
			toastr.warning("Ingresar usuario para continuar", "Portal Clientes");
			return false;
		}
		if (!this.state.password) {
			this.setState({ passwordvalidate: true, textopassword: "Usuario incorrecto" });
			toastr.warning("Ingresar contraseña para continuar", "Portal Clientes");
			return false;
		}

		this.setState({ loading: true });
		var localstorage = window.localStorage;
		var respnseA = axios.post(url.LogIn, {
			Usuario_id: this.state.user,
			Password: this.state.password
		})
			.then(response => response.data)
			.then(data => {
				this.setState({ loading: false });
				if (data.Code != "") {
					localstorage.setItem("Token", data.Code);
					localstorage.setItem("NombreUsuario", data.OData._UsuarioMo.Nombre);
					localstorage.setItem("Usuario_id", data.OData._UsuarioMo.Usuario_id);
					localstorage.setItem("ZEmpleado", data.OData._UsuarioMo.ZEmpleado);
					localstorage.setItem("MenuUsuario", JSON.stringify(data.OData._UsuarioMo._MenuList));
					var rol = _.find(data.OData._UsuarioMo._GrupouList, { Es_activo: true });
					localstorage.setItem("rolusuario", rol ? rol.Grupou_code : "");

					var respnseA = axios.get(url.ParametroFechaActualizacion, { headers: { "Authorization": `Bearer ${data.Code}` } })
						.then(response => response.data)
						.then(data2 => {							
							if (data2.Code == "") {
								localstorage.setItem("FechaActualizacion", data2.OData.Valor);
							} else {
								localstorage.setItem("FechaActualizacion", "");
							}
						})
						.catch(error => {
							console.log(error);
						})

					this.props.history.replace({ pathname: "/home" });
					
				} else {
					toastr.warning(data.Message, "Portal Clientes");
                }
			})
			.catch(error => {
				console.log(error);
				this.setState({ loading: false });
			})

	}
	public RecuperarContraseña = () => {
		this.props.history.replace({ pathname: "/recuperapassword" });
	}
	public render() {
		return < div  >
			<section className="login text-center common-img-bg">
				<Card className="card-login" style={{ borderRadius: '0 0 10px 10px' }}>
					<CardContent>
						<div style={{ alignContent : "middle", textAlign : "center"}}>
						<img src={'/images/LOGO LOGIN.png'} />
						<Typography color="textSecondary" style={{ color: '#0e7ec3', fontSize: '18pt', fontWeight: 'bold' }}>Bienvenido</Typography>
						<Typography color="textSecondary" style={{ color: '#545454', fontSize: '18pt', fontWeight: 'bold' }}>Inicia sesión en tu cuenta</Typography>
				        </div>
					    <br />
						<Grid container item xs={12} xl={12} sm={12} md={12} lg={12}   >
							<TextField id="outlined-u" label="Usuario" variant="outlined" size="small"
								required
								type="text"
								onChange={(evt: any) => { this.setState({ user: evt.target.value }); }}
								onKeyUp={(evt: any) => { if (evt.keyCode == 13) (document.getElementById("outlined-p") as any).focus(); }}
								style={{ width: '100%' }}
								error={this.state.uservalidate}
								helperText={this.state.textouser}
								onFocus={() => {
									this.setState({ uservalidate: false, textouser: "" });
								}}
							/>
						</Grid>
						<br />
						<Grid container item xs={12} xl={12} sm={12} md={12} lg={12}   >
							<TextField id="outlined-p" label="Contraseña" variant="outlined" size="small"
								required
								type="password"
								onChange={(evt: any) => { this.setState({ password: evt.target.value }); }}
								onKeyUp={(evt: any) => { if (evt.keyCode == 13) this.LogIn();}}
								style={{ width: '100%' }}
								error={this.state.passwordvalidate}
								helperText={this.state.textopassword}
								onFocus={() => {
									this.setState({ passwordvalidate: false, textopassword: "" });
								}}
							/>
						</Grid>
						<br />
						<Grid container spacing={2}>
							<Grid item xs={12} xl={12} sm={6} md={6} lg={6}   >
								<div className=" text-left rkmd-checkbox checkbox-rotate checkbox-ripple m-b-25">
									<label className="input-checkbox checkbox-primary">
										<input type="checkbox" id="checkbox" />
										<span className="checkbox"></span>
									</label>
									<div className="captions" style={{color:'#555555'}}>Recuerdame</div>

								</div>
							</Grid>
							<Grid item xs={12} xl={12} sm={6} md={6} lg={6}   >
								<a style={{ color: "#0e7ec3", opacity:'75%' }} onClick={this.RecuperarContraseña} className="text-right"> ¿Olvidaste tu contraseña?</a>
							</Grid>
						</Grid>
						<Grid container item xs={12} xl={12} sm={12} md={12} lg={12}   >
							<button type="button" onClick={this.LogIn} style={{ backgroundColor: '#0e7ec3', color: '#ffffff' }} className="btn-block btn">Iniciar Sesión</button>
						</Grid>
						<br />
						<Grid container spacing={2}>
							<Grid item xs={12} xl={12} sm={6} md={6} lg={6}   >
								<div style={{ color: '#555555' }}>¿No tienes una cuenta?</div>
							</Grid>
							<Grid item xs={12} xl={12} sm={6} md={6} lg={6}   >
								<a style={{ color: "#0e7ec3" }} onClick={() => { }} className="text-right"> Registrate ahora</a>
							</Grid>
						</Grid>
					</CardContent>
				</Card>
		    </section>
			{this.state.loading ? <Loading /> : null}
		</div>;
    }
}