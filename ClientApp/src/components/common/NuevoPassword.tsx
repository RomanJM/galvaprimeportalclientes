﻿
import * as React from 'react';

import { RouteComponentProps } from 'react-router';
import { TextField, Zoom } from '@material-ui/core';
import axios from 'axios';
import * as toastr from './Toast';
import Loading from '../common/Loading';
import * as url from '../../common';

interface NuevoPasswordState {
	password: string,
	loading: boolean,
	passwordvalidate: boolean,
	textopassword: string,
	tokenvalido: boolean,
	token: string
}

export default class NuevoPassword extends React.Component<RouteComponentProps<{}>, NuevoPasswordState>{
	constructor(props: any) {
		super(props);
		this.state = {
			password:"",
			loading: false,
			passwordvalidate: false,
			textopassword: "",
			tokenvalido: false,
			token: ""

		};
	}
	public Enviar = () => {

		if (!this.state.password) {
			this.setState({ passwordvalidate: true, textopassword: "Contraseña incorecta" });
			toastr.warning("Ingresar contraseña para continuar", "Portal Clientes");
			return false;
		}
		console.log("this.state.token", this.state.token);
		console.log("this.state.password", this.state.password);
		this.setState({ loading: true });
		var respnseA = axios.put(url.RecuperarPassword2, {
			Usuario_id: this.state.token,
			Password: this.state.password
		})
			.then(response => response.data)
			.then(data => {
				console.log("data actualiza", data);
				this.setState({ loading: false });
				if (data.Type == "success") {
					toastr.success(data.Message, "Portal Clientes");
				} else {
					toastr.error(data.Message, "Portal Clientes");
				}
			})
			.catch(error => {
				console.log(error);
				this.setState({ loading: false });
			})

	}
	public LogIn = () => {
		this.props.history.replace({ pathname: "/" });
	}
	public ValidaToken = (token: string) => {
		this.setState({ loading: true });
		var respnseA = axios.post(url.ValidaToken, {
			Token: token
		})
			.then(response => response.data)
			.then(data => {
				console.log(data);
				if (data.Type == "success") {
					this.setState({ loading: false, tokenvalido: true });
					toastr.success(data.Message, "Portal Clientes");
				} else {
					this.setState({ loading: false, tokenvalido: false });
					toastr.error(data.Message, "Portal Clientes");
				}
			})
			.catch(error => {
				console.log(error);
				this.setState({ loading: false, tokenvalido: false });
			})
	}
	componentDidMount() {
		const search = (this.props.location as any).search;
		if (search) {
			const params = search.split("?");
			const token = params[1].replace("token=", "");
			this.setState({ token: token });
			this.ValidaToken(token);
        }
    }
	public render() {
		return < div > <section className="login p-fixed d-flex text-center bg-primary common-img-bg">
			<div className="container-fluid">
				<div className="row">
					<div className="col-sm-12">
						<Zoom in={true} timeout={1500} >
							<div className="login-card card-block">
								{this.state.tokenvalido ?
									<form className="md-float-material">
										<div className="text-center">
											<img src={'/images/Logo-Brand.png'} alt="logo" style={{ width: '50%' }} />
										</div>
										<h3 className="text-center text-muted">
											Nueva Contraseña
								        </h3>
										<div className="row">
											<div className="col-md-12">
												<div className="md-input-wrapper">
													<TextField id="outlined-basic" label="Contraseña" variant="outlined"
														required
														type="password"
														onChange={(evt: any) => { this.setState({ password: evt.target.value }); }}
														style={{ width: '100%' }}
														error={this.state.passwordvalidate}
														helperText={this.state.textopassword}
														onFocus={() => {
															this.setState({ passwordvalidate: false, textopassword: "" });
														}}
													/>
												</div>
											</div>
										</div>
										<div className="row">
											<div className="col-xs-10 offset-xs-1">
												<button type="button" onClick={this.Enviar} className="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">Enviar</button>
											</div>
										</div>
										<div className="row">
											<div className="col-xs-10 offset-xs-1">
												<button type="button" onClick={this.LogIn} className="btn btn-info btn-md btn-block waves-effect text-center m-b-20">Iniciar Sesión</button>
											</div>
										</div>
										{/*<div className="col-sm-12 col-xs-12 text-center">*/}
										{/*	<span className="text-muted">¿No tienes una cuenta? </span>*/}
										{/*	<a href="register2.html" className="f-w-600 p-l-5">Regístrese ahora</a>*/}
										{/*</div>*/}

									</form>
									: <div className="text-center"><h3 className="text-center text-muted">
										{'Token no valido'}
								      </h3></div>}
								
							</div>
						</Zoom>
					</div>
				</div>
				
			</div>
		</section> {this.state.loading ? <Loading mensage={'Validando token'} /> : null}</div>;
	}
}