﻿import * as React from 'react';
import LinearBuffer from './LoadingBar';
interface LoadingProps {
    mensage?: string
}
export default class Loading extends React.Component<LoadingProps, any>{
    public render() {
        return <div className="loader-bg">
            <div className="text-center" style={{ marginTop: '25%', color: "#FFFFFF", textAlign: 'center' }}>
                {this.props.mensage}
                <LinearBuffer />
            </div>
            <div className="loader-bar"></div>
        </div>;
    }
}