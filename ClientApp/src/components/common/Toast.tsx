﻿
import * as toastr from 'toastr';
import "toastr/build/toastr.css";
toastr.options.closeButton = true;
toastr.options.debug = false;
toastr.options.newestOnTop = false;
toastr.options.progressBar = true;
toastr.options.positionClass = "toast-bottom-center";
toastr.options.preventDuplicates = false;
toastr.options.showEasing = "swing";
toastr.options.hideEasing = "linear";
toastr.options.showMethod = "fadeIn";
toastr.options.hideMethod = "fadeOut";

export const success = (mensaje: string, title?: string) => {
	toastr.success(mensaje, title);
}
export const error = (mensaje: string, title?: string) => {
	toastr.error(mensaje, title);
}
export const warning = (mensaje: string, title?: string) => {
	toastr.warning(mensaje, title);
}
export const info = (mensaje: string, title?: string) => {
	toastr.info(mensaje, title);
}

