﻿import * as React from 'react';
import { Link } from 'react-router-dom';
import { NavLink } from 'reactstrap';
import * as _ from 'lodash';
import { Fade } from '@material-ui/core';
import { RouteComponentProps } from 'react-router';
import moment from 'moment';
interface NavMenuLeftProps {
    menu: any[],
    fechaActualizacion? : any
}
interface NavMenuLeftState {
    logo: string
}
export default class NavMenuLeft extends React.Component<NavMenuLeftProps, any>{
    constructor(props: any) {
        super(props);
        this.state = {
            logo: "/images/abrir-menu.png"
        };
    }
    public render() {
        console.log("se ejecuta el menuuu");
        var menuheders = _.filter(this.props.menu, (x: any) => {
            return x.Menu_id_padre == 0 && x.Auxx == 1
        });
        return (
            <aside className="main-sidebar hidden-print" style={{ marginTop: '1%' }}>
                <div className="image-menu-fondo" style={{ height: '99vh' }}>
                    <section className="sidebar " id="sidebar-scroll" style={{ paddingTop:'12%' }}>
                        <ul className="sidebar-menu">
                            <li key='-' className="treeview">
                                <NavLink tag={Link}
                                    to={'/'.concat('home')}
                                    style={{ color: '#ffffff' }}
                                    onClick={() => {
                                        var title: any = document.getElementById('title-page');
                                        title.innerHTML = "";
                                    }}
                                >
                                    <i className={'fas fa-home'}></i>
                                    <span>{'Inicio'}</span>
                                </NavLink>
                            </li>
                            {menuheders.map((menu: any) => {
                                var submenus = _.filter(this.props.menu, { Menu_id_padre: menu.Menu_id, Auxx: 1 });
                                //const targetm = menu.Ventana ? 'target='.concat(menu.Ventana) : '';
                                return (
                                    <li key={submenus.length > 0 ? menu.I18n : '#'} className="treeview">
                                        <a href={menu.Enlace} style={{ color: '#ffffff' }} target={menu.Ventana ? menu.Ventana : ''} >
                                            <i className={menu.Imagen}></i>
                                            <span>{menu.I18n}</span>
                                            {
                                                submenus.length > 0 ?
                                                    <i className="fas fa-caret-down icon-arrow-down"></i> : null
                                            }
                                        </a>
                                        {submenus.length > 0 ?
                                            <ul className="treeview-menu" style={{ background: '#0e7ec3', marginRight:'5px' }}>
                                                {
                                                    submenus.map((submenu: any) => {
                                                        return <li key={submenu.I18n}>
                                                            {
                                                                submenu.Ventana ?
                                                                    <a href={submenu.Enlace} style={{ color: '#ffffff' }} target={submenu.Ventana ? submenu.Ventana : ''} >
                                                                        <i className={submenu.Imagen}></i>
                                                                        <span>{submenu.I18n}</span>
                                                                    </a> :
                                                                    <Fade in={true} timeout={1000}>
                                                                        <NavLink
                                                                            tag={Link}
                                                                            to={submenu.Enlace}
                                                                            style={{ color: '#ffffff' }}
                                                                        >
                                                                            <i className={submenu.Imagen}></i>{submenu.I18n}
                                                                        </NavLink>
                                                                    </Fade>
                                                            }
                                                            
                                                        </li>;
                                                    })
                                                }

                                            </ul>
                                            : null}

                                    </li>
                                );
                            })}
                            <li id="ultima_actualizacion" key='/' className="treeview" style={{ position: 'absolute', bottom: "8%" }}>
                                <a>
                                    <span style={{ color: '#ffffff', marginRight: '5px' }} >Última actualización: <br /> <span id="fecha_actualizacion"></span></span>
                                 </a>
                                    </li>
                            {/*<li key='/' className="treeview" style={{ position: 'absolute', bottom:"8%" }}>*/}
                            {/*    <a href="#" style={{ color: '#ffffff' }} onClick={() => {*/}
                            {/*        window.localStorage.clear();*/}
                            {/*        window.location.replace("/");*/}
                            {/*    }}>*/}
                            {/*        <i className={'fas fa-sign-out-alt'}></i>*/}
                            {/*        <span>{'Cerrar sesión'}</span>*/}
                            {/*    </a>*/}
                            {/*</li>*/}
                        </ul>
                    </section>
                </div>
            </aside>
            );
    }
}