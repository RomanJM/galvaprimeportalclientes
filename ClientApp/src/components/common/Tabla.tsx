﻿import * as React from 'react';
import ReactTable from 'react-table';


interface TablaProps {
    registros: any[],
    columnas: any[],
    filterable : boolean
}
export default class Tabla extends React.Component<TablaProps, any>{
    public render() {
        return <div >
            <ReactTable
                data={this.props.registros}
                columns={this.props.columnas}
                showPagination={false}
                showPageSizeOptions={false}
                pageSize={this.props.registros.length}
                filterable={this.props.filterable}
                className="-striped -highlight"
                defaultFilterMethod={(filter, row, column) => {
                    const id = filter.pivotId || filter.id;
                    var text = String(row[id]).toLowerCase();
                    var filtervalue = String(filter.value).toLowerCase();
                    return text !== undefined ? String(text).startsWith(filtervalue) : true;
                }}
                noDataText={"No se encontraron registros"}
            />
        </div>;
    }
}