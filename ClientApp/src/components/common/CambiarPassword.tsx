﻿
import * as React from 'react';

import { RouteComponentProps } from 'react-router';
import { TextField, Zoom } from '@material-ui/core';
import axios from 'axios';
import * as toastr from './Toast';
import Loading from '../common/Loading';
import * as url from '../../common';

interface CambiarPasswordState {
	password: string,
	passwordConfirm : string,
	passwordActual: string,
	passwordActualvalidate: boolean,
	loading: boolean,
	passwordvalidate: boolean,
	passwordConfirmvalidate: boolean,
	textopasswordConfirm : string,
	textopassword: string,
	textopasswordActual :string,
	tokenvalido: boolean,
	token: string
}

export default class CambiarPassword extends React.Component<RouteComponentProps<{}>, CambiarPasswordState>{
	constructor(props: any) {
		super(props);
		this.state = {
			password: "",
			passwordConfirm : "",
			passwordActual : "",
			loading: false,
			passwordvalidate: false,
			passwordActualvalidate : false,
			textopassword: "",
			textopasswordActual : "",
			tokenvalido: false,
			token: "",
			passwordConfirmvalidate: false,
			textopasswordConfirm : ""

		};
	}
	public Enviar = () => {
		if (!this.state.passwordActual) {
			this.setState({ passwordActualvalidate: true, textopasswordActual: "Completar campo contraseña" });
			toastr.warning("Ingresar contraseña actual para continuar", "Portal Clientes");
			return false;
		}
		if (!this.state.password) {
			this.setState({ passwordvalidate: true, textopassword: "Completar campo contraseña" });
			toastr.warning("Ingresar contraseña nueva para continuar", "Portal Clientes");
			return false;
		}
		if (!this.state.passwordConfirm) {
			this.setState({ passwordConfirmvalidate: true, textopasswordConfirm: "Completar campo contraseña" });
			toastr.warning("Confirme la contraseña nueva para continuar", "Portal Clientes");
			return false;
		}
		if (this.state.passwordActual == this.state.password) {
			toastr.warning("Ingrese una contraseña diferente a la actual", "Portal Clientes");
			return false;
		}
		if (this.state.password != this.state.passwordConfirm) {
			toastr.warning("La contraseña nueva es diferente a la contraseña confirmada, por favor verifique su informacion", "Portal Clientes");
			return false;
        }
		this.setState({ loading: true });
		var token = window.localStorage.getItem("Token");
		var respnseA = axios.put(url.ActualizaPassword, {
			Usuario_id: this.state.passwordActual,
			Password: this.state.password
		}, { headers: { "Authorization": `Bearer ${token}` } })
		
			.then(response => response.data)
			.then(data => {
				this.setState({ loading: false });
				if (data.Type == "success") {
					toastr.success(data.Message, "Portal Clientes");
				} else {
					toastr.error(data.Message, "Portal Clientes");
				}
			})
			.catch(error => {
				console.log(error);
				this.setState({ loading: false });
			})

	}
	public LogIn = () => {
		this.props.history.replace({ pathname: "/" });
	}
	public ValidaToken = (token: any) => {
		this.setState({ loading: true });
		var respnseA = axios.post(url.ValidaToken, {
			Token: token
		})
			.then(response => response.data)
			.then(data => {
				console.log(data);
				if (data.Type == "success") {
					this.setState({ loading: false, tokenvalido: true });
					toastr.success(data.Message, "Portal Clientes");
				} else {
					this.setState({ loading: false, tokenvalido: false });
					toastr.error(data.Message, "Portal Clientes");
				}
			})
			.catch(error => {
				console.log(error);
				this.setState({ loading: false, tokenvalido: false });
			})
	}
	componentDidMount() {
		//var token = window.localStorage.getItem("Token");
		//this.ValidaToken(token ? token : "");
	}
	public render() {
		return < div className="text-center" >
			<Zoom in={true} timeout={1500} >
				<div className="login-card card-block" style={{ margin: '0 auto' }}>
					{
						
							<form className="md-float-material">
								<h3 className="text-center text-muted"> Contraseña actual</h3>
								<div className="row">
									<div className="col-md-12">
										<div className="md-input-wrapper">
											<TextField id="outlined-basic" label="Contraseña actual" variant="outlined"
												required
												type="password"
												onChange={(evt: any) => { this.setState({ passwordActual: evt.target.value }); }}
												style={{ width: '100%' }}
												error={this.state.passwordActualvalidate}
												helperText={this.state.textopasswordActual}
												onFocus={() => {
													this.setState({ passwordActualvalidate: false, textopasswordActual: "" });
												}}
											/>
										</div>
									</div>
								</div>
							<h3 className="text-center text-muted"> Nueva contraseña</h3>
							<div className="row">
								<div className="col-md-12">
									<div className="md-input-wrapper">
										<TextField id="outlined-basic" label="Contraseña nueva" variant="outlined"
											required
											type="password"
											onChange={(evt: any) => { this.setState({ password: evt.target.value }); }}
											style={{ width: '100%' }}
											error={this.state.passwordvalidate}
											helperText={this.state.textopassword}
											onFocus={() => {
												this.setState({ passwordvalidate: false, textopassword: "" });
											}}
										/>
									</div>
								</div>
								</div>
								<h3 className="text-center text-muted"> Confirmar contraseña</h3>
								<div className="row">
									<div className="col-md-12">
										<div className="md-input-wrapper">
											<TextField id="outlined-basic" label="Contraseña nueva" variant="outlined"
												required
												type="password"
												onChange={(evt: any) => { this.setState({ passwordConfirm: evt.target.value }); }}
												style={{ width: '100%' }}
												error={this.state.passwordConfirmvalidate}
												helperText={this.state.textopasswordConfirm}
												onFocus={() => {
													this.setState({ passwordConfirmvalidate: false, textopasswordConfirm: "" });
												}}
											/>
										</div>
									</div>
								</div>
							<div className="row">
								<div className="col-xs-10 offset-xs-1">
									<button type="button" onClick={this.Enviar} className="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">Enviar</button>
								</div>
							</div>

						</form> 
					}

				</div>
			</Zoom>
			{this.state.loading ? <Loading mensage={'Validando token'} /> : null}</div>;
	}
}