﻿import * as React from 'react';
import numeral from 'numeral';
import { Chip, Typography } from '@material-ui/core';

interface CardTotalProps {
    onclick: any;
    textoTotal: any;
    label: any;
}
export default class CardTotal extends React.Component<CardTotalProps, any>{
    public render() {
        return <div className={"card "} >
            <div className="card img-fondo-card" onClick={this.props.onclick}>
                <Typography gutterBottom variant="h5" component="h2">
                    <Chip
                        label={numeral(this.props.textoTotal).format("0,0")}
                        color="primary"
                        variant="outlined"
                    />
                </Typography>
                <Typography gutterBottom variant="h6" component="h6">
                    {this.props.label}
                </Typography>
                   <div className="card-body">
                </div>
            </div>          
        </div>;
    }
}