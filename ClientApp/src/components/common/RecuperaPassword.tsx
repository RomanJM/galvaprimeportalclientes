﻿
import * as React from 'react';
import * as url from '../../common';
import { RouteComponentProps } from 'react-router';
import { TextField, Zoom } from '@material-ui/core';
import axios from 'axios';
import * as toastr from './Toast';
import Loading from '../common/Loading';


interface RecuperaPasswordState {
	user: string,
	email: string,
	loading: boolean,
	uservalidate: boolean,
	emailvalidate: boolean,
	textouser: string,
	textoemail: string
}

export default class RecuperaPassword extends React.Component<RouteComponentProps<{}>, RecuperaPasswordState>{
	constructor(props: any) {
		super(props);
		this.state = {
			user: "",
			email: "",
			loading: false,
			uservalidate: false,
			emailvalidate: false,
			textouser: "",
			textoemail: ""

		};
	}
	public Enviar = () => {

		if (!this.state.user) {
			this.setState({uservalidate: true, textouser: "Usuario incorrecto" });
			toastr.warning("Ingresar usuario para continuar", "Portal Clientes");
			return false;
		}
		if (!this.state.email) {
			this.setState({ emailvalidate: true, textoemail: "Email incorrecto" });
			toastr.warning("Ingresar email para continuar", "Portal Clientes");
			return false;
		}

		this.setState({ loading: true });
		var respnseA = axios.post(url.RecuperarPassword, {
			Usuario_id: this.state.user,
			Email: this.state.email
		})
			.then(response => response.data)
			.then(data => {
				this.setState({ loading: false });
				if (data.Type == "success") {
					toastr.success(data.Message, "Portal Clientes");
				} else {
					toastr.error(data.Message, "Portal Clientes");
				}
			})
			.catch(error => {
				console.log(error);
				this.setState({ loading: false });
			})

	}
	public LogIn = () => {
		this.props.history.replace({ pathname: "/" });
	}
	public render() {
		return < div > <section className="login p-fixed d-flex text-center bg-primary common-img-bg">
			<div className="container-fluid">
				<div className="row">
					<div className="col-sm-12">
						<Zoom in={true} timeout={1500} >
							<div className="login-card card-block">
								<form className="md-float-material">
									<div className="text-center">
										<img src={'/images/Logo-Brand.png'} alt="logo" style={{ width: '50%' }} />
									</div>
									<h3 className="text-center text-muted">
										Reccuperar Contraseña
								</h3>
									<div className="row">
										<div className="col-md-12">
											<div className="md-input-wrapper">
												<TextField id="outlined-basic" label="Usuario" variant="outlined"
													required
													type="text"
													onChange={(evt: any) => { this.setState({ user: evt.target.value }); }}
													style={{ width: '100%' }}
													error={this.state.uservalidate}
													helperText={this.state.textouser}
													onFocus={() => {
														this.setState({ uservalidate: false, textouser: "" });
													}}
												/>
											</div>
										</div>
										<div className="col-md-12">
											<div className="md-input-wrapper">
												<TextField id="outlined-basic" label="Email" variant="outlined"
													required
													type="email"
													onChange={(evt: any) => { this.setState({ email: evt.target.value }); }}
													style={{ width: '100%' }}
													error={this.state.emailvalidate}
													helperText={this.state.textoemail}
													onFocus={() => {
														this.setState({ emailvalidate: false, textoemail: "" });
													}}
												/>
											</div>
										</div>
									</div>
									<div className="row">
										<div className="col-xs-10 offset-xs-1">
											<button type="button" onClick={this.Enviar} className="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">Enviar</button>
										</div>
									</div>
									<div className="row">
										<div className="col-xs-10 offset-xs-1">
											<button type="button" onClick={this.LogIn} className="btn btn-info btn-md btn-block waves-effect text-center m-b-20">Iniciar Sesión</button>
										</div>
									</div>
									{/*<div className="col-sm-12 col-xs-12 text-center">*/}
									{/*	<span className="text-muted">¿No tienes una cuenta? </span>*/}
									{/*	<a href="register2.html" className="f-w-600 p-l-5">Regístrese ahora</a>*/}
									{/*</div>*/}

								</form>
							</div>
						</Zoom>
					</div>
				</div>
			</div>
		</section> {this.state.loading ? <Loading /> : null}</div>;
	}
}