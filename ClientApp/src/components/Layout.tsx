import * as React from 'react';
import NavMenuLeft from './common/NavMenuLeft';
import 'react-table/react-table.css';
export default class Layout extends React.Component<any, any>{
    componentDidMount() {
        var menu: any = document.getElementById("menutop");
        menu.style.display = "block";
        var name: any = document.getElementById("NombreUsuario");
        name.innerHTML = window.localStorage.getItem("NombreUsuario");      
    }
    public render() {
        var menu = JSON.parse(String(window.localStorage.getItem("MenuUsuario")));
        var fecha = window.localStorage.getItem("FechaActualizacion");
        return <div>
            <NavMenuLeft menu={menu} fechaActualizacion={fecha} />
            <div className="content-wrapper">
                <div className="container-fluid">
                    <div className="row">
                        <div className="main-header" style={{ marginRight: '10px', backgroundColor:'#ffffff' }}>
                          {this.props.children}
                        </div>
                    </div>

                </div>
            </div>

        </div>;
    }
}
