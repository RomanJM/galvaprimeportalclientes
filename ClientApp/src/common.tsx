﻿
export const UrlServer = "http://clientes.galvaprime.com/web";
//export const UrlServer = "http://localhost:1366";

//GENERAL
export const LogIn = UrlServer.concat('/api/AAcceso');

export const ValidaToken = UrlServer.concat('/api/AAcceso/ValidarToken');

export const ActualizaPassword = UrlServer.concat('/api/AUsuario/ActualizarPasswordx');

export const RecuperarPassword = UrlServer.concat('/api/AAcceso/RecuperarPassword');
export const RecuperarPassword2 = UrlServer.concat('/api/AUsuario/ActualizarPasswordx');

export const ClientesGet = UrlServer.concat('/api/Ocrd/Lista');

export const VendedorGet = UrlServer.concat('/api/Oslp/Lista');

//SEGUIMIENTO OC
export const SeguimientoResumen = UrlServer.concat('/api/DetallesOV/Resumen');

export const SeguimientoExcel = UrlServer.concat('/api/DetallesOV/Excel');

export const SeguimientoDetalle = UrlServer.concat('/api/DetallesOV/Detalle');

//INVENTARIOS
export const InventarioDetalle = UrlServer.concat('/api/Inventario/Detalle');

export const InventarioExcel = UrlServer.concat('/api/Inventario/Excel');

export const InventarioResumen = UrlServer.concat('/api/Inventario/ResumenTipomat ');

export const InventarioResumenAlmacen = UrlServer.concat('/api/Inventario/ResumenAlmacen');

//EMBARQUES
export const EmbarqueDetalle = UrlServer.concat('/api/Embarque/Detalle');

export const EmbarqueResumen = UrlServer.concat('/api/Embarque/Resumen');

export const EmbarqueExcel = UrlServer.concat('/api/Embarque/Excel');

//ESTADO CUENTA
export const EstadoCuentaResumen = UrlServer.concat('/api/Oinv/ResumenEdoCta');

export const EstadoCuentaDetalle = UrlServer.concat('/api/Oinv/edocta');

export const EstadoCuentaExcel = UrlServer.concat('/api/Oinv/EdoctaExcel');

//DOCUMENTOS**************

//FACTURAS

export const FacturaLista = UrlServer.concat('/api/Oinv/Lista');
export const FacturaArchivo= UrlServer.concat('/api/Oinv/ArchivoFactura');

//REMISIONES

export const RemisionLista = UrlServer.concat('/api/Odln/Lista/');
export const RemisionArchivo = UrlServer.concat('/api/Odln/ArchivoRemision/');
export const ReciboArchivo = UrlServer.concat('/api/Odln/ArchivoRecibo/');

//CERTIFICADOS
export const CertificadoLista = UrlServer.concat('/api/Certificados/Lista');
export const CertificadoArchivo = UrlServer.concat('/api/Certificados/ArchivoCertificado');

// DASHBOARD
export const Dashboard = UrlServer.concat('/api/DetallesOV/ResumenInicial');

// ACERCA DE
export const ContactosEmail = UrlServer.concat('/api/AParametro/gerente_em');//email
export const ContactosTelefono = UrlServer.concat('/api/AParametro/gerente_ph');//telefono

//Parametros
export const ParametroFechaActualizacion = UrlServer.concat('/api/AParametro/actualizacion');
